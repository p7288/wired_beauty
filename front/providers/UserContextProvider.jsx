import React, { createContext, useEffect, useState } from 'react';
import { userService } from 'services/user.service';

export const UserContext = createContext(userService.userData);

export const UserContextProvider = (props) => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        const subscription = userService.user.subscribe(x => setUser(x));
        return () => subscription.unsubscribe();
    }, []);

    return (
        <UserContext.Provider value={ user }>
            { props.children }
        </UserContext.Provider>
    )
}