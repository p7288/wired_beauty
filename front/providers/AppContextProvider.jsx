import AlertsList from 'components/Alerts/AlertsList'
import React, { createContext, useEffect, useState } from 'react'
import { UserContextProvider } from './UserContextProvider'
import { AlertContextProvider } from './AlertContextProvider'

export const AppContext = createContext()

export const AppContextProvider = (props) => {

    return (
        <AppContext.Provider value={{}}>
            <UserContextProvider>
                <AlertContextProvider>

                    { props.children }

                </AlertContextProvider>
            </UserContextProvider>
        </AppContext.Provider>
    )
}