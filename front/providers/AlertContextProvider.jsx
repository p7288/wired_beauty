import AlertFloating from 'components/Alerts/AlertFloating'
import React, { createContext, useEffect, useState } from 'react'
import ReactDOM from 'react-dom'

export const AlertContext = createContext({
    alerts: {},
    emptyAlerts: () => {},
    addAlert: () => {},
})

export const AlertContextProvider = (props) => {
    const [alerts, setAlerts] = useState([])
    
    // Ajoute l'alerte associée à une clé donnée
    const addAlert = (type = 'success', content, duration = 5, handleClose = () => {}) => {
        const onClose = (id) => {
            setAlerts(alerts.filter(a => a.id !== id))
            handleClose()
        }
        const newAlert = {id: alerts.length + 1, type, content, duration, onClose}
        setAlerts([...alerts, newAlert])
    }

    // Vide toutes les alertes
    const emptyAlerts = () => {
        setAlerts([])
    }

    return (
        <AlertContext.Provider value={{ alerts, addAlert, emptyAlerts }}>
            <div className="alerts">
                {alerts.map((alert, key) =>
                    <AlertFloating key={key} type={ alert.type } duration={ alert.duration } onClose={() => alert.onClose(alert.id)}>{ alert.content }</AlertFloating>
                )}
            </div>
            { props.children }
        </AlertContext.Provider>
    )
}