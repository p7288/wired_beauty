import { useRouter } from 'next/router'
import DefaultErrorPage from 'next/error'
import { Spinner } from 'components/Animation/Spinner'
import { jsonFetch } from 'functions/api'
import { useEffect } from 'react'
import PageEditor from 'components/PageEditor'

export async function getStaticProps({ params }) {
    const data = await fetch(`${process.env.API_URI}/api/pages/${params.title}`)
  
    return {
        props: { page: await data.json() }
    }
  }

export async function getStaticPaths() {
    return {
        fallback: true,
        paths: []
    }
}

export default function Page({ page }) {
    const router = useRouter()

    if (router.isFallback) {
        return (
            <Spinner />
        )
    }

    if (!page.id) {
        return (
            <DefaultErrorPage statusCode={404} />
        )
    }

    return (
        <>
            <h1>{ page.title }</h1>

            {/* <div dangerouslySetInnerHTML={{__html: page.content}} /> */}
            <PageEditor value={JSON.parse(page.content)} readOnly />
        </>
    )
}