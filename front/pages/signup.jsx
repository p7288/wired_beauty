import { FetchForm, FormField, FormSubmitButton } from "components/Form"
import Link from "components/Link"
import { AlertContext } from "providers/AlertContextProvider"
import { useContext, useState } from "react"
import { userService } from 'services/user.service'
import { useToggle } from 'functions/hooks'
import { SlideIn } from "components/Animation/SlideIn"
import { isAuthenticated } from "functions/auth"
import Router from 'next/router'

export default function Signup () {
    const alerts = useContext(AlertContext)
    const [type, setType] = useState('')

    if (isAuthenticated()) {
        Router.push('/')
    }

    const handleSuccess = async (user) => {
        userService.login(user, true)
        alerts.addAlert('success', 'Registered and logged in', 5, () => {})
    }

    return (
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold">Sign up</h2>
                </div>

                <FetchForm action='/register' className="stack" onSuccess={handleSuccess} method="POST">

                    <FormField name='email' type='email'>
                        Votre email
                    </FormField>
                    <FormField name='password' type='password'>
                        Votre mot de passe
                    </FormField>
                    <select name="type" id="type" onChange={(e) => setType(e.target.value)}
                        className={`appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 rounded-t-md rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                    >
                        <option value="">Select Type...</option>
                        <option value="customer">Customer</option>
                        <option value="tester">Tester</option>
                    </select>
                    <SlideIn show={type === 'customer'}>
                        <label htmlFor="business_name" className="sr-only">Business Name</label>
                        <input type="business_name" id="business_name" name="business_name" autoComplete="business_name" aria-describedby="passwordHelp" placeholder="Enter Business Name"
                            className={`appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 rounded-t-md rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                        />
                    </SlideIn>
                    <SlideIn show={type === 'tester'}>
                        <label htmlFor="firstname" className="sr-only">Firstname</label>
                        <input type="firstname" id="firstname" name="firstname" autoComplete="firstname" aria-describedby="emailHelp" placeholder="Enter firstname"
                            className={`appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                        />
                        <label htmlFor="lastname" className="sr-only">Lastname</label>
                        <input type="lastname" id="lastname" name="lastname" autoComplete="lastname" aria-describedby="passwordHelp" placeholder="Enter lastname"
                            className={`appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}
                        />
                    </SlideIn>

                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/login" exact="true" className="font-medium">Already have an account ? Login</Link>
                        </div>
                    </div>

                    <div>
                        <FormSubmitButton>Register</FormSubmitButton>
                    </div>

                </FetchForm>

            </div>
        </div>
    )
}