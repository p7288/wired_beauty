import { Button } from "components/Button"
import { Modal } from "components/Modal"
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState } from "react"
import Router from 'next/router'
import Link from "components/Link"

export default function Home () {
    return (
        <>
            <h1>La Clinique Digitale est la solution innovante</h1>
            <p>Une App unique qui vous permet de participer de façon ouverte à la construction de votre beauté. Participez en téléchargeant l'application Wired Beauty™ et commencez à construire votre profil beauté. Vous pouvez rejoindre «La Clinique Digitale®» sur différentes thématiques qui vous concernent : l'hydratation est par exemple le premier sujet sur lequel nous nous concentrons, mais aussi très vite le stress oxydant, facteur lié aux rides, par exemple.</p>
            <div className="grid fit">
                <div>
                    <strong>Vos 5 avantages</strong>
                    <ul>
                        <li>Vous comprenez mieux ce dont votre peau et vos cheveux ont besoin.</li>
                        <li>Vous devenez acteur du développement des produits de votre marque préférée ou d’autres marques.</li>
                        <li>Vous pouvez créer votre propre La Clinique Digitale grâce à l'App et y associer d’autres personnes qui partagent les mêmes problèmes que vous.</li>
                        <li>Vous accédez aux solutions de la WB Communauté</li>
                        <li>Vous bénéficiez d’avantages immédiats et dans la durée.</li>
                    </ul>
                </div>

                <div>
                    <strong>Vos 5 engagements</strong>
                    <ul>
                        <li>Les données vous appartiennent.</li>
                        <li>Nous investissons plus de 50% de nos ressources en Recherche et Développement.  </li>
                        <li>Nous sommes neutres.</li>
                        <li>Nous croyons au meilleur de la science et de la nature.</li>
                        <li>Nous construisons ensemble une nouvelle cosmétique.</li>
                    </ul>
                </div>
            </div>
            <br />
            <hr />
            <br />
            <h1>Et bien plus encore</h1>
            <div className="grid fit">
                <div className="text-center">
                    <img className="ma" src="https://images.squarespace-cdn.com/content/v1/5582cba8e4b0e812a1e1f224/1438007201230-ZFOPXE68KG01C16FEY1P/image-asset.png?format=750w" width="250" height="250" />
                    <p>J'échange avec les membres de la WB Communauté qui partagent leurs solutions sur «La Clinique Digitale»</p>
                </div>

                <div className="text-center">
                    <img className="ma" src="https://images.squarespace-cdn.com/content/v1/5582cba8e4b0e812a1e1f224/1438007272310-JW1BFQFN9KGMLV4R7DGY/image-asset.png?format=750w" width="250" height="250" />
                    <p>Je détecte et corrige mes mauvaises habitudes grâce à un coaching sur-mesure</p>
                </div>

                <div className="text-center">
                    <img className="ma" src="https://images.squarespace-cdn.com/content/v1/5582cba8e4b0e812a1e1f224/1438007332207-64Y7Z3TUPOAW324XDSWG/image-asset.jpeg?format=750w" width="250" height="250" />
                    <p>Je bénéficie de promotions exclusives sur mes cosmétiques préférés</p>
                </div>
            </div>
            <Link href='/whatwedo'>What we do</Link>
            <Link href='/whoweare'>Who we are</Link>
        </>
    )
}
