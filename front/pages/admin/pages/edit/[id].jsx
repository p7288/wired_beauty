import { Spinner } from 'components/Animation/Spinner'
import { Button, SubmitButton } from 'components/Button'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import PageEditor from 'components/PageEditor'
import { ApiError, jsonFetch } from 'functions/api'
import { useAsyncEffect } from 'functions/hooks.js'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"
import { userService } from '../../../../services/user.service.js'

export default function Page() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    
    const handleSuccess = () => {
        alerts.addAlert('success', 'Page successfuly created !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/pages/${id}`,{});
            setTitle(data.title)
            setContent(JSON.parse(data.content))
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    const handleSubmit = async () => {
        const id = 11 // userService.userData.id
        setLoading(true)
        try {
            const response = await jsonFetch(`/api/pages/edit/${id}`, { method: 'POST', body: {
                title: title,
                content: JSON.stringify(content),
                admin: `/api/admins/${id}`
            } })
            handleSuccess(response)
        } catch (e) {
            if (e instanceof ApiError) {
                e.violations.forEach(error => {
                    alerts.addAlert('danger', error, 5)
                })
            } else if (e.detail) {
                alerts.addAlert('danger', e.detail, 5, () => {})
            } else {
                alerts.addAlert('danger', e, 5, () => {})
                throw e
            }
        }
        setLoading(false)
    }

    return (
        <>
            <FormField name='title' type='text' value={title} onChange={(e) => { setTitle(e.target.value) }} >
                Page Title
            </FormField>
            <PageEditor name='content' value={content} onChange={(e) => { setContent(e) }} />
            <SubmitButton loading={loading} disabled={loading} onClick={handleSubmit}>
                Create page
            </SubmitButton>
        </>
    )
}