import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"
import styled from 'styled-components'

export default function Pages() {
    const [pages, setPages] = useState([])

    const handleSuccess = async (newPages) => {
        setPages(newPages)
    }

    return (
        <>

            <AdminContainer>
                <Link href={`/admin/pages/new`}>
                    <Button><Icon name="plus"></Icon></Button>
                </Link>
                <FetchTable action='/api/pages' onSuccess={handleSuccess}>
                    <TableHead>
                            <TableRow>
                                <th>ID</th>
                                <th>Page Title</th>
                                <th></th>
                            </TableRow>
                        </TableHead>
                    <TableBody size={9}>
                        {pages.map((page, key) => (
                            <TableRow key={key}>
                                <TableCol>{page.id}</TableCol>
                                <TableCol>{page.title}</TableCol>
                                <TableCol className={"flex"}>
                                    <Link href={`pages/edit/${page.id}`}>
                                        <Button><Icon name="edit"></Icon></Button>
                                    </Link>
                                    <Link href={"#"}>
                                        <Button><Icon name="eye"></Icon></Button>
                                    </Link>
                                    <Link href={"#"}>
                                        <Button><Icon name="trash-2"></Icon></Button>
                                    </Link>
                                </TableCol>
                            </TableRow>
                        ))}
                    </TableBody>
                </FetchTable>
            </AdminContainer>
        </>
    )
}

Pages.requireAuth = true
Pages.requireRole = 'ROLE_ADMIN'
