import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"

export default function Admin() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [admin, setAdmin] = useState({
        email: ''
    })

    const handleSuccess = async (currentAdmin) => {
        setAdmin(currentAdmin)
        alerts.addAlert('success', 'Admin successfuly updated !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/admins/${id}`,{});
            setAdmin(data)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <FetchForm action={'/api/admin/' + id} className="stack" value={admin} onChange={setAdmin} onSuccess={handleSuccess} method="PUT">
            <FormField name='email' type='email' value={admin.email}>
                Admin e-mail
            </FormField>
            <FormSubmitButton>Update</FormSubmitButton>
        </FetchForm>
    )
}