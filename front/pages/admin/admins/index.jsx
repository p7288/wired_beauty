import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"
import styled from 'styled-components';

export default function Admins() {

    const [admins, setAdmins] = useState([])

    const handleSuccess = async (newAdmins) => {
        setAdmins(newAdmins)
    }

    return (
        <AdminContainer>
            <FetchTable action='/api/admins' onSuccess={handleSuccess}>
                <TableHead>
                        <TableRow>
                            <th>ID</th>
                            <th>email</th>
                            <th></th>
                        </TableRow>
                    </TableHead>
                <TableBody size={9}>
                    {admins.map((admin, key) => (
                        <TableRow key={key}>
                            <TableCol>{admin.id}</TableCol>
                            <TableCol>{admin.email}</TableCol>
                            <TableCol className={"flex"}>
                                <Link href={`admins/edit/${admin.id}`}>
                                    <Button><Icon name="edit"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="eye"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="trash-2"></Icon></Button>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
        </AdminContainer>
    )
}

Admins.requireAuth = true
Admins.requireRole = 'ROLE_ADMIN'
