import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"
import styled from 'styled-components';

export default function Products() {
    const [products, setProducts] = useState([])

    const handleSuccess = async (newProducts) => {
        setProducts(newProducts)
    }

    return (
        <AdminContainer>
            <FetchTable action='/api/products' onSuccess={handleSuccess}>
                <TableHead>
                        <TableRow>
                            <th>ID</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th></th>
                        </TableRow>
                    </TableHead>
                <TableBody size={9}>
                    {products.map((product, key) => (
                        <TableRow key={key}>
                            <TableCol>{product.id}</TableCol>
                            <TableCol>{product.name}</TableCol>
                            <TableCol>{product.description}</TableCol>
                            <TableCol className={"flex"}>
                                <Link href={`products/edit/${product.id}`}>
                                    <Button><Icon name="edit"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="eye"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="trash-2"></Icon></Button>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
        </AdminContainer>
    )
}

Products.requireAuth = true
Products.requireRole = 'ROLE_ADMIN'
