import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"

export default function Product() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [product, setProduct] = useState({
        email: ''
    })

    const handleSuccess = async (currentProduct) => {
        setProduct(currentProduct)
        alerts.addAlert('success', 'Product successfuly updated !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/products/${id}`,{});
            setProduct(data)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <FetchForm action={'/api/products/' + id} className="stack" value={product} onChange={setProduct} onSuccess={handleSuccess} method="PUT">
            <FormField name='name' type='text' value={product.name}>
                Product name
            </FormField>
            <FormField name='description' type='textarea' value={product.description}>
                Description
            </FormField>
            <FormSubmitButton>Update</FormSubmitButton>
        </FetchForm>
    )
}