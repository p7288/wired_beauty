import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"
import styled from 'styled-components';

export default function Testers() {

    const [testers, setTesters] = useState([])

    const handleSuccess = async (newTesters) => {
        setTesters(newTesters)
    }

    return (
        <AdminContainer>
            <FetchTable action='/api/testers' onSuccess={handleSuccess}>
                <TableHead>
                        <TableRow>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Birth date</th>
                            <th>Weight</th>
                            <th>Height</th>
                            <th>Position</th>
                            <th>Location</th>
                            <th></th>
                        </TableRow>
                    </TableHead>
                <TableBody size={9}>
                    {testers.map((tester, key) => (
                        <TableRow key={key}>
                            <TableCol>{tester.id}</TableCol>
                            <TableCol>{tester.firstname}</TableCol>
                            <TableCol>{tester.lastname}</TableCol>
                            <TableCol>{new Date(tester.birthDate).toLocaleDateString()}</TableCol>
                            <TableCol>{tester.weight}</TableCol>
                            <TableCol>{tester.height}</TableCol>
                            <TableCol>{tester.position}</TableCol>
                            <TableCol>{tester.location}</TableCol>
                            <TableCol className={"flex"}>
                                <Link href={`testers/edit/${tester.id}`}>
                                    <Button><Icon name="edit"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="eye"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="trash-2"></Icon></Button>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
        </AdminContainer>
    )
}

Testers.requireAuth = true
Testers.requireRole = 'ROLE_ADMIN'
