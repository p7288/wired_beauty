import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"

export default function Tester() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [tester, setTester] = useState({
        email: ''
    })

    const handleSuccess = async (currentTester) => {
        setTester(currentTester)
        alerts.addAlert('success', 'Tester successfuly updated !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/testers/${id}`,{});
            // var tmp_data = data.birthDate;
            // data.birthDate = new Date(tmp_data).toLocaleDateString();
            // console.log(data)
            setTester(data)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <FetchForm action={'/api/testers/' + id} className="stack" value={tester} onChange={setTester} onSuccess={handleSuccess} method="PUT">
            <FormField name='firstname' type='text' value={tester.firstname}>
                First name
            </FormField>
            <FormField name='lastname' type='text' value={tester.lastname}>
                Last name
            </FormField>
            <FormField name='birthDate' type='date' value={new Date(tester.birthDate).toLocaleDateString()}>
                Birth date
            </FormField>
            <FormField name='weight' type='text' value={tester.weight}>
                Weight
            </FormField>
            <FormField name='height' type='text' value={tester.height}>
                Height
            </FormField>
            <FormField name='position' type='text' value={tester.position}>
                Position
            </FormField>
            <FormField name='location' type='text' value={tester.location}>
                Location
            </FormField>
            <FormSubmitButton>Update</FormSubmitButton>
        </FetchForm>
    )
}