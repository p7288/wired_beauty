import { AdminContainer } from "components/Admin/AdminContainer";
import { Card } from "components/Card";
import styled from 'styled-components';

export default function Admin() {
    return (
        <AdminContainer>
            <div className="grid-layout">
                <Card href={`/admin/admins/`} title="Admins" icon="admin">
                    <p>Manage admins of the website</p>
                </Card>
                <Card href={`/admin/pages/`} title="Pages" icon="web-page">
                    <p>Page builder tool</p>
                </Card>
                <Card href={`/admin/customers/`} title="Customers" icon="customer">
                    <p>List of all our customers</p>
                </Card>
                <Card href={`/admin/products/`} title="Products" icon="product">
                    <p>List of all our customers' products</p>
                </Card>
                <Card href={`/admin/campaigns/`} title="Campaigns" icon="campaign">
                    <p>List of all survey created by our customers</p>
                </Card>
                <Card href={`/admin/questions/`} title="Questions" icon="question">
                    <p>Management of all survey's questionnaire</p>
                </Card>
                <Card href={`/admin/testers/`} title="Testers" icon="tester">
                    <p>Database list of all the tester users</p>
                </Card>
            </div>
        </AdminContainer>
    )
}

Admin.requireAuth = true
Admin.requireRole = 'ROLE_ADMIN'

const Container = styled.div`
    .row{
        flex-wrap: wrap; 
        display:flex;
    }

    .row div{
        flex: 0 0 25%;
        width: 100%;
    }

    .row main{
        flex: 0 0 75%;
        width: 100%;
    }

    .grid-layout{
        padding: 1em;
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 10px;
    }
`