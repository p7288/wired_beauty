import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"
import styled from 'styled-components';

export default function Customers() {

    const [customers, setCustomers] = useState([])

    const handleSuccess = async (newCustomers) => {
        setCustomers(newCustomers)
    }

    return (
        <AdminContainer>
            <FetchTable action='/api/customers' onSuccess={handleSuccess}>
                <TableHead>
                        <TableRow>
                            <th>ID</th>
                            <th>Business Name</th>
                            <th>email</th>
                            <th></th>
                        </TableRow>
                    </TableHead>
                <TableBody size={9}>
                    {customers.map((customer, key) => (
                        <TableRow key={key}>
                            <TableCol>{customer.id}</TableCol>
                            <TableCol>{customer.businessName}</TableCol>
                            <TableCol>{customer.email}</TableCol>
                            <TableCol className={"flex"}>
                                <Link href={`customers/edit/${customer.id}`}>
                                    <Button><Icon name="edit"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="eye"></Icon></Button>
                                </Link>
                                <Link href={"#"}>
                                    <Button><Icon name="trash-2"></Icon></Button>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
        </AdminContainer>
    )
}

Customers.requireAuth = true
Customers.requireRole = 'ROLE_ADMIN'
