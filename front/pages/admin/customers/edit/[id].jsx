import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"

export default function Customer() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [customer, setCustomer] = useState({
        email: ''
    })

    const handleSuccess = async (currentCustomer) => {
        setCustomer(currentCustomer)
        alerts.addAlert('success', 'Customer successfuly updated !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/customers/${id}`,{});
            setCustomer(data)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <FetchForm action={'/api/customers/' + id} className="stack" value={customer} onChange={setCustomer} onSuccess={handleSuccess} method="PUT">
            <FormField name='businessName' type='text' value={customer.businessName}>
                Customer Business name
            </FormField>
            <FormField name='email' type='email' value={customer.email}>
                Customer e-mail
            </FormField>
            <FormSubmitButton>Update</FormSubmitButton>
        </FetchForm>
    )
}