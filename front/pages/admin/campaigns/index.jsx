import { AdminContainer } from 'components/Admin/AdminContainer';
import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableColHead, TableHead, TableRow } from "components/Table"
import { jsonFetch } from 'functions/api';
import { useState, useEffect, useContext } from "react"
import styled from 'styled-components';
import {FetchForm, FormField, FormSubmitButton} from "../../../components/Form";
import {AlertContext} from "../../../providers/AlertContextProvider";

export default function campaigns() {
    const alerts = useContext(AlertContext)
    const [campaigns, setCampaigns] = useState([])

    const handleSuccess = async (newCampaigns) => {
        setCampaigns(newCampaigns)
    }

    const handleSuccessUploader = async (currentCampaign) => {
        alerts.addAlert('success', 'Question & answer uploading for this campaign !', 5, () => {})
    }

    const onDeleteCampaign = (id) => {
        const confirm = window.confirm('Do you really want to delete this campaign ?')

        if (confirm) {
            const originalCampaigns  = [...campaigns];

            setCampaigns(campaigns.filter(campaign => campaign.id !== id))

            jsonFetch(`/api/campaigns/${id}`, { method: 'DELETE' })
                .then(() => {
                    setCampaigns(campaigns.filter(campaign => campaign.id !== id))
                    alerts.addAlert('success', 'Campaign has been delete.', 5, () => {})
                })
                .catch(() => {
                    setCampaigns(originalCampaigns);
                    alerts.addAlert('danger', "Could'nt delete campaign", 5, () => {});
                })
        }
    }


    return (
        <AdminContainer>
            <FetchTable action='/api/campaigns?groups=campaign:admin-view' onSuccess={handleSuccess}>
                <TableHead>
                        <TableRow>
                            <TableColHead>ID</TableColHead>
                            <TableColHead>Name</TableColHead>
                            <TableColHead>Product</TableColHead>
                            <TableColHead>Business name</TableColHead>
                            <TableColHead className="w-72">Actions</TableColHead>
                        </TableRow>
                    </TableHead>
                <TableBody size={5}>
                    {campaigns.map((campaign, key) => (
                        <TableRow key={key} className="border-b border-white">
                            <TableCol>{campaign.id}</TableCol>
                            <TableCol>{campaign.label}</TableCol>
                            <TableCol>{campaign.product.name}</TableCol>
                            <TableCol>{campaign.product.customer.business_name}</TableCol>
                            <TableCol className="grid grid-cols-3 items-center justify-center gap-2">
                                <Link href={`campaigns/edit/${campaign.id}`} className="justify-self-center !px-2 !py-2 !bg-transparent !text-indigo-500">
                                    <Icon name="edit-2"></Icon>
                                </Link>
                                <FetchForm action={'/api/uploader/import-questions-config-file'} className="flex items-center justify-center gap-2 justify-self-center" value={campaign} onSuccess={handleSuccessUploader} method="POST" contentType="multipart/form-data">
                                    <div className='hidden'>
                                        <FormField name='campaignId' type='text' value={campaign.id} className={"hidden hidden-input m-0 p-0"}></FormField>
                                    </div>
                                    <div className='flex items-center' style={{marginTop: '6px'}}>
                                        <FormField name='file' type='file' customLabelClass="custom-file-upload">
                                            <Icon name="upload"></Icon>
                                        </FormField>
                                    </div>
                                    <div className='flex items-center h-full'>
                                        <FormSubmitButton className="!px-4 !py-2">Send</FormSubmitButton>
                                    </div>
                                </FetchForm>
                                <Link href={"#"} className='justify-self-center' onClick={() => onDeleteCampaign(campaign.id)} >
                                    <Icon name="trash-2"></Icon>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
        </AdminContainer>
    )
}

campaigns.requireAuth = true
campaigns.requireRole = 'ROLE_ADMIN'
