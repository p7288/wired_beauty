import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext,useState, useEffect } from "react"

export default function Campaign() {
    const alerts = useContext(AlertContext)
    const router = useRouter()
    const [id, setId] = useState(router.query.id)
    const [loading, setLoading] = useState(false)
    const [campaign, setCampaign] = useState({
        email: ''
    })

    const handleSuccess = async (currentCampaign) => {
        setCampaign(currentCampaign)
        alerts.addAlert('success', 'Campaign successfuly updated !', 5, () => {})
    }

    useEffect(async () => {
        if (!id) return

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/campaigns/${id}&groups=campaign:admin-view`,{});
            setCampaign(data)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [id])

    useEffect(() => {
        // The id changed!
        setId(router.query.id)
    }, [router.query.id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <FetchForm action={'/api/campaigns/' + id} className="stack" value={campaign} onChange={setCampaign} onSuccess={handleSuccess} method="PUT">
            <FormField name='label' type='text' value={campaign.label}>
                Campaign name
            </FormField>
            <FormField name='status' type='text' value={campaign.status}>
                Status
            </FormField>
            <FormField name='clientDesc' type='textarea' value={campaign.clientDesc}>
                Customer description
            </FormField>
            <FormField name='testerDesc' type='textarea' value={campaign.testerDesc}>
                Tester description
            </FormField>
            <FormField name='productQuantity' type='number' min={1} value={campaign.productQuantity}>
                Product quantity
            </FormField>
            <FormSubmitButton>Update</FormSubmitButton>
        </FetchForm>
    )
}