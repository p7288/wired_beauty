import { AdminContainer } from 'components/Admin/AdminContainer';

export default function Questions() {
    return (
        <AdminContainer>
            <h1>hello questions</h1>
        </AdminContainer>
    )
}

Questions.requireAuth = true
Questions.requireRole = 'ROLE_ADMIN'
