import React from 'react'

export default function WhatWeDo () {
    return (
        <>
            <div>
                <h1 className="text-center text-3xl font-bold">What we do</h1><br/>
                <h2 className="text-2xl font-bold underline">BeautyTech X Skincare</h2>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">WE RESEARCH SKINCARE</h3><br/>
                            <p>WIRED BEAUTY is a new datadriven market research company dedicated to cosmetic laboratories.</p>
                            <p>
                                We help our clients evaluate and develop skincare formulae by comparing cosmetics <span className="underline">on statistically viable samples</span> of men & women (1000 people) :
                                <ul className="list-disc ml-7">
                                    <li>The antioxydative and moisterizing effect of formulae (in vivo)</li>
                                    <li>The antioxydative and moisterizing effect of ingredients (in vitro)  (cf Reach)</li>
                                    <li>Consumer feedback after product use</li>
                                    <li>Environmental conditions (UV pollution) surronding the consumer’s lifestyle</li>
                                    <li>Skincare performance after 1 day – 8 days – 15 days - X days.</li>
                                </ul>
                            </p>
                        </div>
                        <div className="grid place-items-center">
                            <img src="/images/wwd-skincare.png" alt="www skincare" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div className="grid place-items-center">
                            <img src="/images/wwd-iot.png" alt="www iot" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">WE RELY ON DATA & TECHNOLOGY</h3><br/>
                            <ol className="list-decimal ml-7">
                                <li>Proprietary nomadic lab device & patented skin patches to decipher skin reactions and ingredient presence</li>
                                <li>Easy multi-lingual smartphone app and customer feedback</li>
                                <li>Geolocalisation and exposome data (UV - pollution - temperature)</li>
                                <li>Repeatability of protocols in real life conditions (ie bathrooms)</li>
                                <li>Large cohorts it needed</li>
                                <li>Saas and European data center</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">WE WORK FOR LABS AND MARKETING TEAMS</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>Combining efforts with countries usual CRO’s</li>
                                    <li>In all countries, even in China where anonymized data is key</li>
                                </ul>
                            </p>
                        </div>
                        <div className="grid place-items-center">
                            <img src="/images/wwd-micro.png" alt="www micro" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div className="grid place-items-center">
                            <img src="/images/wwd-help.png" alt="www help" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">WE HELP</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li><span className="font-bold">DISCOVER</span> new claims on large skin type cohorts</li>
                                    <li><span className="font-bold">PROVE</span> claims through large data intelligence</li>
                                    <li><span className="font-bold">TRULY UNDERSTAND</span> skin reactions in absence of product</li>
                                    <li><span className="font-bold">COMPARE</span> performance on a long-term basis (Cohorts follow-up)</li>
                                    <li><span className="font-bold">UNDERSTAND</span> exposome and skin</li>
                                    <li><span className="font-bold">SAVE TIME</span> vs chromatography and <span className="font-bold">IMROVE</span> results vs TWEL.</li>
                                    <li><span className="font-bold">READ SIMULTANOUSLY SKIN</span> response & consumer feed-back</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

WhatWeDo.requireAuth = true
