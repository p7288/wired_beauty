import React from 'react'

export default function WhoWeAre () {
    return (
        <>
            <div>
                <h1 className="text-center text-3xl font-bold">What we are</h1><br/>
                <h1 className="text-2xl font-bold">Les valeurs que nous partageons</h1>
                <div className="grid grid-cols-3 mt-5">
                    <div className="flex mt-5 flex-start">
                        <img src="/images/validate.png" alt="sophie" className="object-cover h-24 w-46 mr-10"/>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">Conformité / Sécurité</h3>
                            <p className="w-80">Les objets connectés de WB Technologies sont certifiés conformes aux normes européennes de sécurité en vigueur.</p>
                        </div>
                    </div>
                    <div className="flex mt-5 flex-start">
                        <img src="/images/graph.png" alt="sophie" className="object-cover h-24 w-46 mr-10"/>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">Partage / Ouverture</h3>
                            <p className="w-80">Sans anonymisation, aucune donnée personnelle n'est analysée.</p>
                        </div>
                    </div>
                    <div className="flex mt-5 flex-start">
                        <img src="/images/bit.png" alt="sophie" className="object-cover h-24 w-46 mr-10"/>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">Confidentialité / Sérénité</h3>
                            <p className="w-80">Vous disposez d’un libre accès et droit de suppression sur l’ensemble de vos données.</p>
                        </div>
                    </div>
                    <div className="flex mt-5 flex-start">
                        <img src="/images/lumiere.png" alt="sophie" className="object-cover h-24 w-46 mr-10"/>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">Indépendance / Intelligence</h3>
                            <p className="w-80">Nous sommes indépendants de toute marque, chacun, y compris vous, a son rôle dans la construction de votre beauté, nous faisons le maximum pour mériter votre confiance.</p>
                        </div>
                    </div>
                    <div className="flex mt-5 flex-start">
                        <img src="/images/lock.png" alt="sophie" className="object-cover h-24 w-46 mr-10"/>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">Confidentialité</h3>
                            <p className="w-80">Vos données personnelles sont stockées sur des serveurs sécurisés.</p>
                        </div>
                    </div>
                </div>
                <br/>
                <h1 className="text-2xl font-bold">Qui sommes nous</h1>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">SOPHIE LE TANNEUR</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>An FMCG corporate life of over 20 years at Colgate-Palmolive & L’Oréal in marketing and general management positions in Europe.</li>
                                    <li>An entrepreneurial life with  turn-arounds of a luxury brands backed by Private equity  funds for followed by 7 years of consulting @Lucinda which she founded.</li>
                                    <li>A passion for entrepreneurship as an impact investor and mentor.</li>
                                </ul>
                            </p>
                        </div>
                        <div className="grid place-items-center">
                            <img src="/images/sophie.png" alt="sophie" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div className="grid place-items-center">
                            <img src="/images/stanislas.png" alt="stanislas" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">STANISLAS VANDIER</h3><br/>
                            <ol className="list-disc ml-7">
                                <li>10 years at L’Oréal in marketing and general management positions in Europe.</li>
                                <li>An entrepreneurial life in Europe and Asia in dermo-cosmetics (product development and alternative retail models) and data / AI.</li>
                                <li>A passion for innovation and team ventures.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">NICOLAS AMALRIC</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>Scientific Director of QIMA Life Sciences and former CEO of Synelvia</li>
                                    <li>QIMA Life Sciences (formerly Bioalternatives & Synelvia) offre des services d’analyses adaptés à la biologie cutanée. L’offre de tests s’étend du screening virtuel in silico à la bioanalyse d’échantillons cliniques non invasifs, en passant par les tests in vitro et ex vivo.
                                        La société QIMA Life Sciences aide les départements marketing et recherche et développement des grands groupes cosmétique et pharmaceutique européens et américains à innover dans la conceptualisation de leurs projets et à substancier et soutenir leurs allégations produits.
                                    </li>
                                    <li>A passion for innovation in skin science.</li>
                                </ul>
                            </p>
                        </div>
                        <div className="grid place-items-center">
                            <img src="/images/nicolas.png" alt="nicolas" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div className="grid place-items-center">
                            <img src="/images/william.png" alt="william" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">WILLIAM RICHARD</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>Directeur d'étudesQIMA Life Sciences Toulouse France since 2018 - Développement de capteurs</li>
                                    <li>Responsable département électrochimie</li>
                                    <li>Gestion de projets</li>
                                    <li>Capteurs</li>
                                    <li>Matériaux</li>
                                    <li>Chimie analytique</li>
                                    <li>Qualité</li>
                                    <li>A passion for biology and tech</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">TU-ANH DUONG</h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>Dermatologist, APHP, Hôpital Henri Mondor</li>
                                    <li>TELDERM</li>
                                    <li>Ecole Centrale Paris , Interne des hopitaux de Paris, PhD</li>
                                    <li>A passion for trans-disciplinary innovation</li>
                                </ul>
                            </p>
                        </div>
                        <div className="grid place-items-center">
                            <img src="/images/tu-rnh.png" alt="tu rnh" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                    </div>
                </div>
                <div className="m-10">
                    <div className="grid grid-cols-2 mt-5">
                        <div className="grid place-items-center">
                            <img src="/images/melissa.png" alt="william" className="object-cover h-48 w-96 rounded-md"/>
                        </div>
                        <div>
                            <h3 className="text-1xl mt-5 font-bold">MELISSA EVEILLARD
                            </h3><br/>
                            <p>
                                <ul className="list-disc ml-7">
                                    <li>Trainee at APHP (May’ 21) as a data scientist for the Wired beauty project and SKINBIOSENSE clinical trial and  efficiency vs chromatography.</li>
                                    <li>Master in Engineering, Mathematics, and Biostatistics track @ Paris Universty</li>
                                    <li>A passion for new technologies, data exploration and communication based on deep analysis.</li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

WhoWeAre.requireAuth = true
