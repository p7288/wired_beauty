import Link from "components/Link"
import React, { useState, useEffect, useContext } from "react"
import { AlertContext } from "providers/AlertContextProvider"
import { FetchForm, FormField, FieldTextarea, FormSubmitButton } from "components/Form";
import { jsonFetch } from 'functions/api'
import { Spinner } from "components/Animation/Spinner";
import Router, { useRouter } from 'next/router'

export default function ProductUpdate() {
    const alerts = useContext(AlertContext)
    const router = useRouter();
    const [product, setProduct] = useState({});
    const [loading, setLoading] = useState(true)
    const [id, setId] = useState(router.query.id);

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The product has been updated', 5, () => {});
        await Router.push('/customers/products');
    }

    const fetchProduct = async id => {
        setLoading(true)
        try {
            const response = await jsonFetch(`/api/products/${id}`);
            setProduct(response)
        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    } 

    useEffect(() => {
        setId(router.query.id)
    }, [router.query.id])

    useEffect(() => {
        if (!id) return
        fetchProduct(id)
    }, [id])

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Creation of a new product</h2>
                    </div>
                    <FetchForm action={`/api/products/${id}`} className="stack" value={product} onSuccess={handleSuccess} method="PUT">
                        <FormField name='name' type='text' value={product.name}>
                            Name of your product
                        </FormField>
                        <FormField name='description' type='textarea' value={product.description}>
                            Description of your product
                        </FormField>
                        <div className="py-1">
                            <FormSubmitButton>Create</FormSubmitButton>
                        </div>
                    </FetchForm>
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/customers/products" exact="true" className="font-medium">Return to products list</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
ProductUpdate.requireAuth = true
ProductUpdate.requireRole = 'ROLE_CUSTOMER'