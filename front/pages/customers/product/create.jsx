import Link from "components/Link"
import React, { useState, useEffect, useContext } from "react"
import { AlertContext } from "providers/AlertContextProvider"
import { FetchForm, FormField, FieldTextarea, FormSubmitButton } from "components/Form";
import Router from 'next/router'
import { userInfo } from "../../../services/user.service";
import { Spinner } from "../../../components/Animation/Spinner";

export default function ProductCreate() {
    const alerts = useContext(AlertContext)
    const [product, setProduct] = useState({
        name: ""
    });
    const [user, setUser] = useState();

    useEffect(async () => {
        if (user) return
        setUser(await userInfo());
    }, []);

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The product has been created', 5, () => {});
        await Router.push('/customers/products');
    }

    if (!user) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Creation of a new product</h2>
                    </div>
                    <FetchForm action={'/api/products'} className="stack" value={product} onSuccess={handleSuccess} method="POST">
                        <FormField name='name' type='text'>
                            Name of your product
                        </FormField>
                        <FormField name='description' type='textarea'>
                            Description of your product
                        </FormField>
                        <FormField name='customer' type='hidden' value={`/api/customers/${user.id}`} />
                        <div className="py-1">
                            <FormSubmitButton>Create</FormSubmitButton>
                        </div>
                    </FetchForm>
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/customers/products" exact="true" className="font-medium">Return to products list</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

ProductCreate.requireAuth = true
ProductCreate.requireRole = 'ROLE_CUSTOMER'