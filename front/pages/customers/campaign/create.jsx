import React, {useState, useEffect, useContext} from "react"
import { FetchForm, FormField, FormSelect, FormSubmitButton, FieldTextarea} from "components/Form"
import Link from "components/Link"
import { jsonFetch } from '/functions/api.js'
import { AlertContext } from "providers/AlertContextProvider"
import Router from 'next/router'
import { userService } from "services/user.service"
import { userInfo } from "services/user.service";
import { Spinner } from "components/Animation/Spinner";
import { margin } from "tailwindcss/defaultTheme"

export default function CampaignCreate () {
    const [user, setUser] = useState();
    useEffect(async () => {
        if (user)return
        setUser(await userInfo());
    }, []);

    const alerts = useContext(AlertContext)
    const [campaign, setCampaign] = useState({
        label: "",
        product: "",
        status: "",
        customer: ""
    });
    const [products, setProducts] = useState([])

    const allStatus = [
        "done",
        "to_validate",
        "in_progress"
    ]

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The campaign has been created', 5, () => {});
        await Router.push('/customers/campaigns');
    }

    const handleChange = ({ currentTarget }) => {
        const {label, value} = currentTarget;
        setCampaign({ ...campaign, [label]:value })
    }; 

    const fetchProducts = async () => {
        try {
            const data = await jsonFetch(`/api/products`);
            setProducts(data);
        } catch (e) {
            alerts.addAlert('danger', "Could't get the products", 5, () => {});
        }
    }

    useEffect(() => {
        fetchProducts();
    }, [])

    if (!user) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Creation of a new campaign</h2>
                    </div>
                    <FetchForm action={'/api/campaigns'} className="stack" data={{customer:"/api/customers/"+user.id}} value={campaign} onSuccess={handleSuccess} method="POST">
                        <FormField name='label' type='text'>
                            Name of your campaign
                        </FormField>
                        <FormField name='clientDesc' type='textarea' label="Product number">
                            Client description
                        </FormField>
                        <FormField name='testerDesc' type='textarea'>
                            Tester description
                        </FormField>
                        <div style={{margin: '1rem 0'}}>
                            <FormSelect name="status" label="Status" onChange={handleChange}>
                                {allStatus.map(status => <option key={status} value={status}>{status}</option>)}
                            </FormSelect>
                        </div>
                        <div style={{margin: '1rem 0'}}>
                            <FormSelect id="product" name="product" label="Product" onChange={handleChange}>
                                {products.map(product => <option key={product.id} value={"/api/products/" + product.id}>{product.name}</option>)}
                            </FormSelect>
                        </div>
                        <FormField name='productQuantity' type='number' min={1}>
                            Product quantity
                        </FormField>
                        <div className="py-5 py-1">
                            <FormSubmitButton>Create</FormSubmitButton>
                        </div>
                    </FetchForm>
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/customers/campaigns" exact="true" className="font-medium">Return to campaigns list</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

CampaignCreate.requireAuth = true
CampaignCreate.requireRole = 'ROLE_CUSTOMER'