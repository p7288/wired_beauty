import React, {useState, useEffect, useContext} from "react"
import {FetchForm, FormField, FormSubmitButton} from "components/Form"
import Link from "components/Link"
import { jsonFetch } from '/functions/api.js'
import Router, { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { Spinner } from "components/Animation/Spinner"

export default function SesionUpdate () {
    const alerts = useContext(AlertContext)
    const router = useRouter();
    const [loading, setLoading] = useState(false)
    const [id, setId] = useState(router.query.id);
    const [session, setSession] = useState({
        instructions: "",
        quantity: "",
        dateEnd: "",
        dateBegin: "",
        isPlacebo: false
    });

    useEffect(() => {
        setId(router.query.id)
    }, [router.query.id])

    useEffect(() => {
        if (!id) return
        fetchSession(id)
    }, [id])

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The session has been updated', 5, () => {});
        await Router.push(`/customers/campaign/${session.campaign.id}`);
    }

    const fetchSession = async id => {
        setLoading(true)
        try {
            const data = await jsonFetch(`/api/sessions/${id}`);
            setSession(data);
        } catch {
            alerts.addAlert('danger', "Could'nt get sessions", 5, () => {});
        }
        setLoading(false)
    }

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Updating a session</h2>
                    </div>
                    <FetchForm action={`/api/sessions/${session.id}`} className="stack" value={session} onSuccess={handleSuccess} method="PUT">
                        <div style={{margin: '1rem 0'}}>
                            <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                            >
                                Session start
                            </label>
                            <input type="date" id="start" name="dateBegin" defaultValue={session.dateBegin.substring(0, session.dateBegin.indexOf('T'))} className="
                                form-control
                                block
                                w-full
                                px-3
                                py-1.5
                                text-base
                                font-normal
                                text-gray-700
                                bg-white bg-clip-padding
                                border border-solid border-gray-300
                                rounded
                                transition
                                ease-in-outs
                                m-0
                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                            ">
                            </input>
                        </div>
                        <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                        >
                            Session end
                        </label>
                        <input type="date" id="end" name="dateEnd" defaultValue={session.dateEnd.substring(0, session.dateEnd.indexOf('T'))} className="
                                form-control
                                block
                                w-full
                                px-3
                                py-1.5
                                text-base
                                font-normal
                                text-gray-700
                                bg-white bg-clip-padding
                                border border-solid border-gray-300
                                rounded
                                transition
                                ease-in-outs
                                m-0
                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                            ">
                        </input>
                        <FormField name='instructions' type='text' defaultValue={session.instructions}>
                            Instructions
                        </FormField>
                            <FormField name='quantity' min={1} type='number' defaultValue={session.quantity}>
                                <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                                >
                                    Number of tester
                                </label>
                            </FormField>
                            <div className="text-center">
                                <label htmlFor="placebo" className="form-label inline-block text-gray-700"
                                >
                                    Placebo ?
                                </label>
                                <div className="flex justify-center">
                                    <FormField name='isPlacebo' type='checkbox' defaultValue={session.isPlacebo}>
                                    </FormField>
                                </div>
                            </div>
                        <div className="py-5 py-1">
                            <FormSubmitButton>Update</FormSubmitButton>
                        </div>
                    </FetchForm>
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            {
                                session.campaign
                                &&
                                (
                                    <Link href={`/customers/campaign/${session.campaign.id}`} exact="true" className="font-medium">Return to the campaign</Link>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

SesionUpdate.requireAuth = true
SesionUpdate.requireRole = 'ROLE_CUSTOMER'