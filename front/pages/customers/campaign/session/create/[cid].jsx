import React, {useState, useEffect, useContext} from "react"
import {FetchForm, FormField, FormSubmitButton} from "components/Form"
import Link from "components/Link"
import { AlertContext } from "providers/AlertContextProvider"
import Router, {useRouter} from "next/router";
import {Spinner} from "../../../../../components/Animation/Spinner";
import {jsonFetch} from "../../../../../functions/api";

export default function SessionCreate () {
    const alerts = useContext(AlertContext);
    const [session, setSession] = useState({});
    const router = useRouter();
    const [cid, setCid] = useState(router.query.cid);
    const [loading, setLoading] = useState(false);
    const [campaign, setCampaign] = useState({
        id: ""
    });

    useEffect(() => {
        setCid(router.query.cid)
    }, [router.query.cid])

    useEffect(() => {
        if (!cid) return
        fetchCampaign(cid)
    }, [cid])

    const fetchCampaign = async cid => {
        setLoading(true)
        try {
            const data = await jsonFetch(`/api/campaigns/${cid}`);
            console.log(data);
            setCampaign(data);
        } catch {
            alerts.addAlert('danger', "Could'nt get campaign", 5, () => {});
        }
        setLoading(false)
    }

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The session has been created', 5, () => {});
        await Router.push(`/customers/campaign/${campaign.id}`);
    };

    if (!cid) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Creating a session</h2>
                    </div>
                    <FetchForm action={`/api/sessions`} className="stack" data={{campaign:"/api/campaigns/"+campaign.id}} value={session} onSuccess={handleSuccess} method="POST">
                        <div style={{margin: '1rem 0'}}>
                            <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                            >
                                Session start
                            </label>
                            <input type="date" id="start" name="dateBegin" className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-1.5
                                    text-base
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border border-solid border-gray-300
                                    rounded
                                    transition
                                    ease-in-outs
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                ">
                            </input>
                        </div>
                        <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                        >
                            Session end
                        </label>
                        <input type="date" id="end" name="dateEnd" className="
                                form-control
                                block
                                w-full
                                px-3
                                py-1.5
                                text-base
                                font-normal
                                text-gray-700
                                bg-white bg-clip-padding
                                border border-solid border-gray-300
                                rounded
                                transition
                                ease-in-outs
                                m-0
                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                            ">
                            </input>
                            <div style={{margin: '1rem 0'}}>
                                <FormField name='instructions' type='textarea'>
                                    Instructions
                                </FormField>
                            </div>
                            <div>
                                <FormField name='quantity' type='number' min={1}>
                                    <div className="mb-1 xl:w-96">
                                        <label htmlFor="quantity" className="form-label inline-block text-gray-700"
                                        >
                                            Number of tester
                                        </label>
                                    </div>
                                </FormField>
                            </div>
                            <div className="text-center">
                                <label htmlFor="placebo" className="form-label inline-block text-gray-700"
                                >
                                    Placebo ?
                                </label>
                                <div className="flex justify-center">
                                    <FormField name='isPlacebo' type='checkbox'>
                                    </FormField>
                                </div>
                            </div>
                        <div className="py-5 py-1">
                            <FormSubmitButton>Create</FormSubmitButton>
                        </div>
                    </FetchForm>
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            {
                                campaign &&
                                (
                                    <Link href={`/customers/campaign/${campaign.id}`} exact="true" className="font-medium">Return to the campaign</Link>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

SessionCreate.requireAuth = true
SessionCreate.requireRole = 'ROLE_CUSTOMER'