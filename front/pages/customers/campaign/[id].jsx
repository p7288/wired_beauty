import React, {useState, useEffect, useContext} from "react"
import { FetchForm, FormField, FormSelect, FormSubmitButton } from "components/Form"
import Link from "components/Link"
import { jsonFetch } from '/functions/api.js'
import Router, { useRouter } from 'next/router'
import { AlertContext } from "providers/AlertContextProvider"
import { Spinner } from "components/Animation/Spinner"
import { Icon } from "../../../components/Icon";
import { Button } from "../../../components/Button";

export default function CampaignUpdate () {
    const alerts = useContext(AlertContext)
    const router = useRouter();
    const [loading, setLoading] = useState(false)
    const [id, setId] = useState(router.query.id);
    const [sessions, setSessions] = useState([])
    const [campaign, setCampaign] = useState({
        label: "",
        product: "",
        status: "",
        clientDesc: "",
        testerDesc: "",
        productQuantity: 1
    });
    const [products, setProducts] = useState([])
    const allStatus = [
        "to_validate",
        "done",
        "in_progress"
    ]

    useEffect(() => {
        setId(router.query.id)
    }, [router.query.id])

    useEffect(() => {
        if (!id) return
        fetchCampaign(id)
    }, [id])

    useEffect(() => {
        fetchProducts();
    }, [])

    const onDeleteSession = (sessionId) => {
        const confirm = window.confirm('Do you really want to delete this session');

        if (confirm) {
            jsonFetch(`/api/sessions/${sessionId}`, { method: 'DELETE' })
                .then(res => {
                    setSessions(sessions.filter(session => session.id !== sessionId));
                    alerts.addAlert('success', 'Session has been delete.', 5, () => {});
                })
                .catch(() => {
                    alerts.addAlert('danger', "Couldn't delete the session", 5, () => {});
                })
        }
    }

    const handleSuccess = async () => {
        alerts.addAlert('success', 'The campaign has been updated', 5, () => {});
        await Router.push(`/customers/campaigns}`);
    }

    const fetchProducts = async () => {
        setLoading(true)
        try {
            const data = await jsonFetch(`/api/products`);
            setProducts(data);
        } catch {
            alerts.addAlert('danger', "Could'nt get products", 5, () => {});
        }
        setLoading(false)
    }

    const fetchCampaign = async id => {
        setLoading(true)
        try {
            const data = await jsonFetch(`/api/campaigns/${id}`);
            setCampaign(data);
            setSessions(data.sessions);
        } catch {
            alerts.addAlert('danger', "Could'nt get products", 5, () => {});
        }
        setLoading(false)
    }

    if (!id || loading) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12">
                <div className="w-7/12 space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Updating of campaign</h2>
                    </div>
                    <FetchForm action={`/api/campaigns/${id}`} className="stack" value={campaign} onSuccess={handleSuccess} method="PUT">
                        <FormField name='label' type='text' defaultValue={campaign.label}>
                            Name of your campaign
                        </FormField>

                        <FormField name='clientDesc' type='textarea' label="Product number" defaultValue={campaign.clientDesc}>
                            Client description
                        </FormField>
                        <FormField name='testerDesc' type='textarea' defaultValue={campaign.clientDesc}>
                            Tester description
                        </FormField>
                        <div style={{margin: '1rem 0'}}>
                            <FormSelect name="status" label="Status" defaultValue={campaign.status}>
                                {allStatus.map(status => <option key={status} defaultValue={status}>{status}</option>)}
                            </FormSelect>
                        </div>
                        <div style={{margin: '1rem 0'}}>
                            <FormSelect id="product" name="product" label="Product" defaultValue={campaign.product}>
                                {products.map(product => <option key={product.id} value={"/api/products/" + product.id}>{product.name}</option>)}
                            </FormSelect>
                        </div>
                        <FormField name='productQuantity' type='number' min={1} value={campaign.productQuantity}>
                                <div className="mb-1 xl:w-96">
                                    <label htmlFor="productQuantity" className="form-label inline-block text-gray-700"
                                    >
                                        Product quantity
                                    </label>
                                </div>
                        </FormField>
                        <div className="flex justify-center px-6 py-3">
                            <label className="form-label inline-block text-gray-700"
                            >
                                Sessions
                            </label>
                            <div className="px-6 py-3">
                                <Link exact="true" button href={`/customers/campaign/session/create/${campaign.id}`}>
                                    <Icon name="plus-circle">
                                    </Icon>
                                </Link>
                            </div>
                        </div>
                        <table className="table-auto rounded-lg border-collapse border border-slate-500 border-opacity-50">
                            <thead>
                            <tr>
                                <th className="px-6 py-3 text-center">Date begin</th>
                                <th className="px-6 py-3 text-center">Date end</th>
                                <th className="px-6 py-3 text-center">Quantity</th>
                                <th className="px-6 py-3 text-center">Instructions</th>
                                <th className="px-6 py-3 text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                campaign.sessions
                                &&
                                (
                                    sessions.map((session, key) => (
                                        <tr className="bg-white border-b" key={key}>
                                            <td className="px-6 py-3">{
                                                session.dateBegin
                                                    ?
                                                    session.dateBegin.substring(0, session.dateBegin.indexOf('T'))
                                                    :
                                                    ""
                                            }</td>
                                            <td className="px-6 py-3 text-center">{
                                                session.dateEnd
                                                    ?
                                                    session.dateEnd.substring(0, session.dateEnd.indexOf('T'))
                                                    :
                                                    ""
                                            }</td>
                                            <td className="px-6 py-3 text-center">{session.quantity}</td>
                                            <td className="px-6 py-3 text-center">{session.instructions}</td>
                                            <td className="px-6 py-3 !flex gap-2 text-center">
                                                <Link href={`/customers/campaign/session/${session.id}`} className="!px-2 !py-2 !bg-transparent !text-indigo-500">
                                                    <Icon name="edit-2"/>
                                                </Link>
                                                <Button className="!px-2 !py-2 !bg-transparent !text-red-500" onClick={() => onDeleteSession(session.id)} >
                                                    <Icon name="trash"/>
                                                </Button>
                                            </td>
                                        </tr>
                                    ))
                                )
                            }
                            </tbody>
                        </table>
                        <div className="py-5 py-1">
                            <FormSubmitButton>Update</FormSubmitButton>
                        </div>
                    </FetchForm>
                
                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/customers/campaigns" exact="true" className="font-medium">Return to campaigns list</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

CampaignUpdate.requireAuth = true
CampaignUpdate.requireRole = 'ROLE_CUSTOMER'