import React, {useContext, useEffect, useState} from 'react'
import { FetchTable, TableBody, TableCol, TableColHead, TableHead, TableRow } from 'components/Table'
import { Button } from 'components/Button'
import { Icon } from 'components/Icon'
import { jsonFetch } from 'functions/api'
import { Pagination } from 'components/Pagination'
import Link from 'components/Link'
import { AlertContext } from "providers/AlertContextProvider"
import { userInfo } from "../../services/user.service";
import { Spinner } from "../../components/Animation/Spinner";

export default function Products () {
  const alerts = useContext(AlertContext)
  const [products, setProducts] = useState([])
  const [search, setSearch] =  useState('');
  const [currentPage, setCurrentPage] = useState((1));
  const [user, setUser] = useState();

  const filteredProducts = products.filter(c => c.name.toLowerCase().includes(search.toLocaleLowerCase()));
  const itemsPerPage = 10;
  const paginatedProducts = Pagination.getData(filteredProducts, currentPage, itemsPerPage);

  useEffect(async () => {
    if (user) return
    setUser(await userInfo());
  }, []);

  const handleSuccess = (products) => {
    setProducts(products);
  }

  const handleChangePage = (page) => {
    setCurrentPage(page);
  }

  const handleSearch = event => {
      const value = event.currentTarget.value;
      setSearch(value);
      setCurrentPage(1);
  }
  
  const onDeleteProduct = (productId) => {
    const confirm = window.confirm('Do you really want to delete this product ?')

    if (confirm) {
      jsonFetch(`/api/products/${productId}`, { method: 'DELETE' })
        .then(() => {
          setProducts(products.filter(product => product.id !== productId))
          alerts.addAlert('success', 'Product has been delete.', 5, () => {});
        })
        .catch(() => {
          alerts.addAlert('danger', "Could'nt delete the product. Check if you deleted all the campaigns with this product first", 5, () => {});
        })
    }
  }

  if (!user) {
      return <Spinner />
  }

  return (
    <>
      <div className="flex justify-center">
        <div className="mb-3 xl:w-96">
          <div className="input-group relative flex flex-wrap items-stretch w-full mb-4">
            <input onChange={handleSearch} value={ search } type="search" className="form-control relative flex-auto min-w-0 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-indigo-500 focus:outline-none" placeholder="Search ..." aria-label="Search" aria-describedby="button-addon2"/>
            <div className="mx-auto mt-5">
                <Link exact="true" button href="/customers/product/create">Create a new product</Link>
            </div>
          </div>
        </div>
      </div>
      <FetchTable action={`/api/products?customer=${user.id}`} onSuccess={handleSuccess} className="max-w-3xl mx-auto">
        <TableHead>
        <TableRow className="border-b border-white">
            <TableColHead>ID</TableColHead>
            <TableColHead>Nom</TableColHead>
            <TableColHead>Description</TableColHead>
            <TableColHead className="w-24">Actions</TableColHead>
          </TableRow>
        </TableHead>
        <TableBody size={4}>
          {paginatedProducts.map((product, key) => (
            <TableRow key={key} className="border-b border-white">
              <TableCol>{product.id}</TableCol>
              <TableCol>{product.name}</TableCol>
              <TableCol>{product.description}</TableCol>
              <TableCol className="grid grid-cols-2 gap-2">
                <Link href={`/customers/product/${product.id}`} className="!px-2 !py-2 !bg-transparent !text-indigo-500">
                    <Icon name="edit-2"/>
                </Link>
                <Button className="!px-2 !py-2 !bg-transparent !text-red-500" onClick={() => onDeleteProduct(product.id)} >
                  <Icon name="trash" />
                </Button>
              </TableCol>
            </TableRow>
          ))}
        </TableBody>
      </FetchTable>
        {
        itemsPerPage < filteredProducts.length
          && 
          (
            <Pagination currentPage={currentPage} itemsPerPage={itemsPerPage} length={filteredProducts.length} onPageChanged={handleChangePage}/>
          )
          }
    </>
  )
}

Products.requireAuth = true
Products.requireRole = 'ROLE_CUSTOMER'