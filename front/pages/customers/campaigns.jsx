import { FetchTable, TableBody, TableCol, TableColHead, TableHead, TableRow } from "components/Table"
import { jsonFetch } from '/functions/api.js'
import React, {useContext, useEffect, useState} from "react"
import { Button } from 'components/Button'
import { Icon } from "components/Icon";
import { Pagination } from "components/Pagination";
import Link from "components/Link"
import { userInfo } from "../../services/user.service";
import { Spinner } from "../../components/Animation/Spinner";
import { AlertContext } from "../../providers/AlertContextProvider";

export default function Campaigns () 
{
    const alerts = useContext(AlertContext)
    const [campaigns, setCampaigns] = useState([]);
    const [currentPage, setCurrentPage] = useState((1));
    const [search, setSearch] =  useState("");
    const [user, setUser] = useState();

    useEffect(async () => {
        if (user) return
        setUser(await userInfo());
    }, []);

    const onDeleteCampaign = (id) => {
        const confirm = window.confirm('Do you really want to delete this campaign ?')

        if (confirm) {
            jsonFetch(`/api/campaigns/${id}`, { method: 'DELETE' })
                .then(() => {
                    setCampaigns(campaigns.filter(campaign => campaign.id !== id));
                    alerts.addAlert('success', 'Campaign has been delete.', 5, () => {});
                })
                .catch(() => {
                    alerts.addAlert('danger', "Could'nt delete campaign", 5, () => {});
                })
        }
    }

    const handleSuccess = async (newCampaigns) => {
        setCampaigns(newCampaigns);
    }

    const handleChangePage = (page) => {
        setCurrentPage(page);
    }

    const handleSearch = event => {
        const value = event.currentTarget.value;
        setSearch(value);
        setCurrentPage(1);
    }

    const itemsPerPage = 10;

    const filteredCampaigns = campaigns.filter(
        c => 
            c.label.toLowerCase().includes(search.toLocaleLowerCase())
            ||
            c.product.name.toLowerCase().includes(search.toLocaleLowerCase())
            ||
            c.status.toLowerCase().includes(search.toLocaleLowerCase())
            ||
            c.clientDesc.toLowerCase().includes(search.toLocaleLowerCase())
            ||
            c.testerDesc.toLowerCase().includes(search.toLocaleLowerCase())

    );

    const paginatedCampaigns = Pagination.getData(filteredCampaigns, currentPage, itemsPerPage);

    if (!user) {
        return <Spinner />
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Campaigns list</h2>
                    </div>
                </div>
            </div>
            <div className="flex justify-center">
                <div className="mb-3 xl:w-96">
                    <div className="input-group relative flex flex-wrap items-stretch w-full mb-4">
                        <input onChange={ handleSearch } value={ search } type="search" className="form-control relative flex-auto min-w-0 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-indigo-500 focus:outline-none" placeholder="Search ..." aria-label="Search" aria-describedby="button-addon2"/>
                        <div className="mx-auto mt-5">
                            <Link exact="true" button href="/customers/campaign/create">Create a new campaign</Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-end mr-150">
            </div>
            <FetchTable action={`/api/campaigns?customer=${user.id}`} onSuccess={handleSuccess} className="max-w-3xl mx-auto">
                <TableHead>
                    <TableRow className="border-b border-white">
                        <TableColHead>ID</TableColHead>
                        <TableColHead>Label</TableColHead>
                        <TableColHead>Product name</TableColHead>
                        <TableColHead>Status</TableColHead>
                        <TableColHead>Sessions</TableColHead>
                        <TableColHead>Survey</TableColHead>
                        <TableColHead>Client description</TableColHead>
                        <TableColHead>Tester description</TableColHead>
                        <TableColHead>Actions</TableColHead>
                    </TableRow>
                </TableHead>
                <TableBody size={9}>
                    {paginatedCampaigns.map((campaign, key) => (
                        <TableRow key={key} className="border-b border-white">
                            <TableCol>{campaign.id}</TableCol>
                            <TableCol>{campaign.label}</TableCol>
                            <TableCol>{campaign.product.name}</TableCol>
                            <TableCol>{campaign.status}</TableCol>
                            <TableCol>{campaign.sessions.length}</TableCol>
                            <TableCol>{campaign.survey ? campaign.survey.name : "NULL"}</TableCol>
                            <TableCol>{campaign.clientDesc}</TableCol>
                            <TableCol>{campaign.testerDesc}</TableCol>
                            <TableCol className="grid grid-cols-2 gap-2">
                                <Link href={`/customers/campaign/${campaign.id}`} className="!px-2 !py-2 !bg-transparent !text-indigo-500">
                                    <Icon name="edit-2"/>
                                </Link>
                                <Button className="!px-2 !py-2 !bg-transparent !text-red-500" onClick={() => onDeleteCampaign(campaign.id)} >
                                    <Icon name="trash"/>
                                </Button>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
            {
            itemsPerPage < filteredCampaigns.length
             && 
             (
               <Pagination currentPage={currentPage} itemsPerPage={itemsPerPage} length={filteredCampaigns.length} onPageChanged={handleChangePage}/>
             )
             }
        </>
    )
}

Campaigns.requireAuth = true
Campaigns.requireRole = 'ROLE_CUSTOMER'