import 'styles/app.css'
import { useState, useEffect, createContext } from 'react'
import Header from 'components/Header'
import Head from 'next/head'

import { AppContextProvider } from 'providers/AppContextProvider'
import AuthGuard from 'components/AuthGuard'
import { useRouter } from 'next/router'

export default function App({ Component, pageProps }) {
    const router = useRouter()
    useEffect(() => {
        // The id changed!
        // setId(router.query.id)
    }, [router.query])

    return (
        <AppContextProvider>
            <div className="App">
                <Head>
                    <title>Wired Beauty </title>
                    <link rel="icon" href="/favicon.ico" />
                </Head>

                {/* Header */}
                <Header />

                <main className='p-5'>
                    {/* if requireAuth property is present - protect the page */}
                    {Component.requireAuth ? (
                        <AuthGuard role={Component.requireRole}>
                            <Component {...pageProps} />
                        </AuthGuard>
                    ) : (
                        // public page
                        <Component {...pageProps} />
                    )}
                </main>

                {/* Footer */}
                {/* <Footer /> */}
            </div>
        </AppContextProvider>
    )
}
