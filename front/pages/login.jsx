import { FetchForm, FormField, FormSubmitButton } from "components/Form"
import Link from "components/Link"
import { AlertContext } from "providers/AlertContextProvider"
import { useContext, useState, useEffect } from "react"
import { userService } from 'services/user.service'
import { useToggle } from 'functions/hooks'
import { isAuthenticated } from "functions/auth"
import Router from 'next/router'
import * as Yup from 'yup'

export default function Signin () {
    const alerts = useContext(AlertContext)
    const [rememberMe, toggleRememberMe] = useToggle(false)

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email("email invalide")
            .required("l'email est obligatoire"),
        password: Yup.string()
            .required("Mot de passe est obligatoire")
            .min(8, "Mot de passe doit être plus grand que 8 caractères")
            .max(50, "Mot de passe doit être plus petit que 50 caractères"),
    });

    if (isAuthenticated()) {
        Router.push('/')
    }

    const handleSuccess = async (user) => {
        userService.login(user, rememberMe)
        alerts.addAlert('success', 'Logged in', 5, () => {})
    }

    return (
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold">Sign in</h2>
                </div>

                <FetchForm action='/login' className="stack" validationSchema={validationSchema} onSuccess={handleSuccess} method="POST">

                    <FormField name='email' type='email'>
                        Votre email
                    </FormField>
                    <FormField name='password' type='password'>
                        Votre mot de passe
                    </FormField>

                    <div className="flex items-center justify-between">
                        <div className="flex items-center">
                            <input id="remember_me" name="remember_me" type="checkbox"
                                className="h-4 w-4 focus:ring-indigo-500 border-gray-300 rounded" onChange={toggleRememberMe} defaultChecked={rememberMe} />
                            <label htmlFor="remember_me" className="ml-2 block">
                                Remember me
                            </label>
                        </div>

                        <div className="text-sm">
                            <a href="#" className="font-medium">
                                Forgot your password?
                            </a>
                        </div>
                    </div>

                    <div className="flex items-center justify-end">
                        <div className="text-sm">
                            <Link href="/signup" exact="true" className="font-medium">Doesn't have an account ? Sign up</Link>
                        </div>
                    </div>

                    <div>
                        <FormSubmitButton>Login</FormSubmitButton>
                    </div>

                </FetchForm>

            </div>
        </div>
    )
}