import { Spinner } from 'components/Animation/Spinner'
import { FetchForm, FormField, FormSubmitButton } from 'components/Form'
import { jsonFetch } from 'functions/api'
import { AlertContext } from "providers/AlertContextProvider"
import { useContext, useState, useEffect } from "react"
import { toFormatDate } from 'functions/dom'
import styled from 'styled-components';

const Tata = styled.div`
    form{
        max-width:50%;
        margin: 0 auto;
    }
    .form-group{
        display: flex;
        flex-align: row;
        align-items: center;
    }
    .form-group input{
        flex: 1 0 0px;
        margin-left:1em;
    }
`

export default function Tester() {
    const alerts = useContext(AlertContext)
    const [tester, setTester] = useState({})
    const [tester_id, setTesterId] = useState({})
    const [loading, setLoading] = useState(false)

    const handleSuccess = async (currentTester) => {
        setTester(currentTester)
        alerts.addAlert('success', 'Tester successfuly updated !', 5, () => { })
    }

    useEffect(async () => {

        setLoading(true)
        try {
            const data = await jsonFetch(`/api/me`);
            setTesterId(data.id)
            setTester(data)

        } catch (e) {
            console.log(e)
        }
        setLoading(false)
    }, [])

    if (loading) {
        return <Spinner />
    }

    const handleChange = event => {
        let date = new Date(event.target.value)
        event.target.value = toFormatDate(date);
    };

    return (
        <Tata>
            <FetchForm action={'/api/testers/' + tester_id} className="stack" value={tester} onChange={setTester} onSuccess={handleSuccess} method="PUT">
                <FormField name='firstname' type='text' value={tester.firstname}>
                    First name
                </FormField>
                <FormField name='lastname' type='text' value={tester.lastname}>
                    Last name
                </FormField>
                <FormField name='birthDate' type='date' onChange={handleChange} defaultValue={toFormatDate(tester.birthDate) }>
                    Birth date
                </FormField>
                <FormField name='weight' type="number" step="0.01" value={tester.weight}>
                    Weight (Kg)
                </FormField>
                <FormField name='height' type="number" step="0.01" value={tester.height}>
                    Height (M)
                </FormField>
                <FormField name='position' type='text' value={tester.position}>
                    Position
                </FormField>
                <FormField name='location' type='text' value={tester.location}>
                    Postal adress
                </FormField>
                <FormSubmitButton id="submit">Update</FormSubmitButton>
            </FetchForm>
        </Tata>

    )
}