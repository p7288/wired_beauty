import { FetchTable, TableBody, TableCol, TableColHead, TableHead, TableRow } from "components/Table"
import { AlertContext } from "providers/AlertContextProvider"
import { jsonFetch } from '/functions/api.js'
import { useContext,useState } from "react"
import { Button } from 'components/Button'
import { Icon } from "components/Icon";
import { Pagination } from "components/Pagination";
import Link from "components/Link"
import AlertFloating from 'components/Alerts/AlertFloating'
import TagSelect from "components/Select";
import { useEffect } from "react";

export default function Campaigns() {
    const alerts = useContext(AlertContext)
    const [campaigns, setCampaigns] = useState([]);
    const [currentPage, setCurrentPage] = useState((1));
    const [tags, setTags] = useState([]);
    const [search, setSearch] = useState("");
    const [successMessage, setSuccessMessage] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const handleSuccess = async (newCampaigns) => {
        setCampaigns(newCampaigns);
    }

    const handleChangePage = (page) => {
        setCurrentPage(page);
    }

    const handleSearch = event => {
        if (typeof event.length != "undefined") {
            if (event.length != 0) {
                let tmp_tags = []
                event.forEach(element => {
                    tmp_tags.push(element.value)
                });
                setTags(tmp_tags);
            }
            else {
                setTags([])
            }
        }
        else {
            setSearch(event.currentTarget.value);
        }
        setCurrentPage(1);
    }

    const itemsPerPage = 10;

    const filteredCampaigns = campaigns.filter(c => {
        console.log(c)
        let display =
            c.product.name.toLowerCase().includes(search.toLocaleLowerCase())
        if (!display) {
            return false
        }

        if (tags.length > 0) {
            display = false
            if (typeof c.product.tags.find(element => tags.includes(element.name)) !== "undefined") {
                display = true
            }
        }
        return display
    })

    const paginatedCampaigns = Pagination.getData(filteredCampaigns, currentPage, itemsPerPage);

    const showSuccessMessage = () => {
        if (successMessage) {
            return (
                <AlertFloating duration={1}>{successMessage}</AlertFloating>
            )
        }
    }

    const showErrorMessage = () => {
        if (errorMessage) {
            return (
                <AlertFloating duration={1} type="danger">{errorMessage}</AlertFloating>
            )
        }
    }

    const handleClick = (e) => {
        var session_id = e.currentTarget.getAttribute('data_campaign_id');
        jsonFetch(`/api/register/${session_id}`, { method: 'GET' })
        .then(res => {
            setSessions(sessions.filter(session => session.id !== sessionId));
            alerts.addAlert('success', 'You have been successfully registered', 5, () => {});
        })
        .catch(() => {
            alerts.addAlert('danger', "Could'nt register you to this campaign", 5, () => {});
        })
    }

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <h2 className="mt-6 text-center text-3xl font-extrabold">Campaigns list</h2>
                    </div>
                </div>
            </div>

            <div className="flex justify-center">
                <div className="mb-3 xl:w-96">
                    <div className="input-group relative flex flex-wrap items-stretch w-full mb-4">
                        <input onChange={handleSearch} value={search} type="search" className="form-control relative flex-auto min-w-0 block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-indigo-500 focus:outline-none" placeholder="Search ..." aria-label="Search" aria-describedby="button-addon2" />
                    </div>
                    <div className="input-group relative flex flex-wrap items-stretch w-full mb-4">
                        <label>Filter by Tags</label>
                        <TagSelect onChange={handleSearch} action="/api/tags" />
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-end mr-150">

            </div>
            <FetchTable action="/api/campaigns/getavailable" onSuccess={handleSuccess} className="max-w-3xl mx-auto">
                <TableHead>
                    <TableRow className="border-b border-white">
                        <TableColHead>Product name</TableColHead>
                        <TableColHead>Description</TableColHead>
                    </TableRow>
                </TableHead>
                <TableBody size={9}>
                    {paginatedCampaigns.map((campaign, key) => (
                        <TableRow key={key} className="border-b border-white">
                            <TableCol>{campaign.product.name}</TableCol>
                            <TableCol>{campaign.testerDesc}</TableCol>
                            <TableCol className="grid grid-cols-2 gap-2">
                                <button onClick={handleClick} data_campaign_id={campaign.sessions[0].id} className="!px-2 !py-2 !bg-transparent !text-indigo-500">
                                    <Icon name="register" />
                                </button>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable>
            {
                itemsPerPage < filteredCampaigns.length
                &&
                (
                    <Pagination currentPage={currentPage} itemsPerPage={itemsPerPage} length={filteredCampaigns.length} onPageChanged={handleChangePage} />
                )
            }
        </>
    )
}

Campaigns.requireAuth = true
// Campaigns.requireRole = 'ROLE_TESTER'