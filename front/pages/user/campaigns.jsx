import { Button } from 'components/Button';
import { Icon } from 'components/Icon';
import Link from 'components/Link';
import { FetchTable, TableBody, TableCol, TableHead, TableRow } from "components/Table"
import { useState, useEffect } from "react"

export default function campaigns() {

    const [campaigns, setCampaigns] = useState([])

    const handleSuccess = async (newCampaigns) => {
        setCampaigns(newCampaigns)
    }

    return (
        <>
            {
                campaigns.length <= 0 &&
                <>
                    <h2>
                        You are not register in any campaign click this link bellow to show a campaign.
                    </h2>
                    <Link href={`/user/listCampaigns`}>
                    click here
                    </Link>
                </>
            }
            < FetchTable action='/api/campaigns/mycampaigns' onSuccess={handleSuccess} >
                <TableHead>
                    <TableRow>
                        <th>Name</th>
                        <th>Product</th>
                        <th>Business name</th>
                        <th></th>
                    </TableRow>
                </TableHead>
                <TableBody size={9}>
                    {campaigns.map((campaign, key) => (
                        <TableRow key={key}>
                            <TableCol>{campaign.label}</TableCol>
                            <TableCol>{campaign.product.name}</TableCol>
                            <TableCol>{campaign.product.customer.business_name}</TableCol>
                            <TableCol className={"flex"}>
                                <Link href={"#"}>
                                    <Button><Icon name="survey"></Icon></Button>
                                </Link>
                            </TableCol>
                        </TableRow>
                    ))}
                </TableBody>
            </FetchTable >
        </>

    )
}

campaigns.requireAuth = true
campaigns.requireRole = 'ROLE_TESTER'
