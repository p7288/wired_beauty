const colors = require('tailwindcss/colors')

module.exports = {
    content: [
        './pages/**/*.{js,ts,jsx,tsx}',
        './components/**/*.{js,ts,jsx,tsx}'
    ],
    theme: {
        extend: {
            colors: {
                ...colors,
                "primary": "var(--primary)",
                "secondary": "var(--secondary)",
            }
        },
    },
    plugins: [
        require('@tailwindcss/forms')
    ],
}