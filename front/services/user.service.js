import { BehaviorSubject } from 'rxjs'
import Router from 'next/router'
import jwt_decode from 'jwt-decode'
import { jsonFetch } from 'functions/api'

const userSubject = new BehaviorSubject(process.browser && JSON.parse(
    localStorage.getItem('user') || sessionStorage.getItem('user')
))

export const userService = {
    user: userSubject.asObservable(),
    get userValue () { return userSubject.value },
    get userData () { return userSubject.value ? jwt_decode(userSubject.value.token) : '' },
    login,
    logout,
    isExpiredJWT,
    refreshJWT
}

async function refreshJWT() {
    const url = `/token/refresh?refresh_token=${userService.userValue.refresh_token}`
    const user = await jsonFetch(url, { method: 'GET' }, false)
    if (localStorage.getItem('user')) {
        localStorage.setItem('user', JSON.stringify(user))
    } else {
        sessionStorage.setItem('user', JSON.stringify(user))
    }
    return userSubject.next(user)
}

function isExpiredJWT() {
    return (Math.floor((new Date).getTime() / 1000)) >= userService.userData.exp;
}

export async function userInfo() {
    return await jsonFetch("/api/me");
}

async function login(user, rememberMe) {
    // publish user to subscribers and store in local storage to stay logged in between page refreshes
    if (rememberMe) {
        localStorage.setItem('user', JSON.stringify(user))
    } else {
        sessionStorage.setItem('user', JSON.stringify(user))
    }
    userSubject.next(user)
    if (typeof Router.query.redirect !== 'undefined') {
        return Router.push(`/${Router.query.redirect}`)
    }
    Router.push('/')
}

async function logout() {
    // remove user from local storage, publish null to user subscribers and redirect to login page
    localStorage.removeItem('user')
    sessionStorage.removeItem('user')
    userSubject.next(null)
    /* Redirecting the user to the home page. */
    await Router.push('/')
}