import { Icon } from "components/Icon";
import Link from "components/Link";

export function SideBar() {
    return (
        <aside aria-label="Sidebar">
            <div className="overflow-y-auto py-4 px-3 bg-gray-50 rounded dark:bg-gray-800">
                <ul className="space-y-2">
                    <li>
                        <Link href="/admin/admins" className="inline-flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='admin'/>
                            <span className="ml-3">Admins</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/pages" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='web-page' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Pages</span>
                            <span className="inline-flex justify-center items-center px-2 ml-3 text-sm font-medium text-gray-800 bg-gray-200 rounded-full dark:bg-gray-700 dark:text-gray-300">Pro</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/customers" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='customer' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Customers</span>
                            <span className="inline-flex justify-center items-center p-3 ml-3 w-3 h-3 text-sm font-medium text-blue-600 bg-blue-200 rounded-full dark:bg-blue-900 dark:text-blue-200">3</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/products" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='product' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Products</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/campaigns" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='campaign' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Campaigns</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/questions" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='question' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Questions</span>
                        </Link>
                    </li>
                    <li>
                        <Link href="/admin/testers" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <Icon name='tester' />
                            <span className="flex-1 ml-3 whitespace-nowrap">Testers</span>
                        </Link>
                    </li>
                </ul>
            </div>
        </aside>
    );
}
