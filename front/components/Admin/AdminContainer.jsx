import {SideBar} from "./SideBar";
import styled from 'styled-components';

export function AdminContainer({children,...props}) {
    return (
        <Container>
            <div className="row">
                <SideBar/>
                <main>
                    {children}
                </main>
            </div>
        </Container>
    )
}

const Container = styled.div`
    .row{
        flex-wrap: wrap; 
        display:flex;
    }

    .row div{
        flex: 0 0 25%;
        width: 100%;
    }

    .row main{
        flex: 0 0 75%;
        width: 100%;
    }

    .grid-layout{
        padding: 1em;
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 10px;
    }
`