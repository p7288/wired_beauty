import React, { createContext, useEffect, useLayoutEffect, useState, useContext, useMemo } from 'react'
import Select from 'react-select'
import { ApiError, jsonFetch } from '/functions/api.js'
import { AlertContext } from 'providers/AlertContextProvider'

/**
 * Table Ajax
 *
 * @param {object} value Donnée à transmettre au serveur et aux champs
 * @param onChange
 * @param {string} action URL de l'action à appeler
 * @param {bool} floatingAlert
 * @param onSuccess Fonction appelée en cas de retour valide de l'API (reçoit les données de l'API en paramètre)
 */
export default function TagSelect({
    action,
    onChange,
    onSuccess = () => {}
}) {
    const alerts = useContext(AlertContext)
    const [tags, setTags] = useState([]);
    const [{ loading, errors }, setState] = useState({
        loading: false,
        errors: []
    })

    // On charge les éléments dès l'affichage du composant
    useEffect(async () => {
        setState({ loading: true, errors: [] })
        try {
            const response = await jsonFetch(action, { method: 'GET' })
            let tags = [];
            response.forEach(element => {
                tags.push({'value':element.name,'label':element.name})
            });
            setTags(tags)
            onSuccess(response)
        } catch (e) {
            if (e instanceof ApiError) {
                setState(s => ({ ...s, errors: e.violations }))
            } else if (e.detail) {
                alerts.addAlert('danger', e.detail, 5, () => { })
            } else {
                alerts.addAlert('danger', e, 5, () => { })
                throw e
            }
        }
        setState(s => ({ ...s, loading: false }))
    }, [])


    return (
        <Select options={tags} isMulti onChange={onChange}>
        </Select>
    )
}