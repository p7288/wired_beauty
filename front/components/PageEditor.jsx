import { useEffect, useState, useContext } from 'react'
import Editor from '@react-page/editor' // The editor core
import '@react-page/editor/lib/index.css' // import the main css
import slate from '@react-page/plugins-slate' // The rich text area plugin
import image from '@react-page/plugins-image' // image
import '@react-page/plugins-slate/lib/index.css' // Stylesheets for the rich text area plugin
import '@react-page/plugins-image/lib/index.css' // Stylesheets for the imagea plugin
import { createTheme } from '@material-ui/core/styles'

// Define which plugins we want to use.
const cellPlugins = [slate(), image]

export default function PageEditor({ value, ...props }) {
    
    return (
        <>
            <Editor cellPlugins={cellPlugins} value={value} {...props} />
        </>
    )
}