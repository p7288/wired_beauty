import styled from "styled-components"

/**
 * Icône basé sur la sprite SVG
 * @param {{name: string}} props
 */
export function Icon ({ name, size = 24 }) {
  const className = `icon icon-${name} w-full md:w-auto`
  const href = `/sprite.svg#${name}`
  return (
    <svg className={className} width={size} height={size}>
      <use xlinkHref={href} />
    </svg>
  )
}