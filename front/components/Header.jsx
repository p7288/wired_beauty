import { useContext, useState } from 'react'
import { UserContext } from 'providers/UserContextProvider'
import React from 'react'
import LoginButton from '/components/LoginButton'
import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'
import ThemeSwitcher from 'components/ThemeSwitcher'
import Link from 'components/Link'
import { hasRole } from "functions/auth"
import { Button } from './Button'
import { Popup } from './Popup'
import styled from 'styled-components'

const MenuNav = styled.div`
    .popup-dialog{
        width: 150px;
    }
    .popup-box{
        display: flex;
        flex-direction: column;
        align-items: stretch;
        padding: 0;
    }
    .popup-box > button{
        border-radius: 0.375rem 0.375rem 0 0;
    }
    .popup-box > a {
        width: 100%;
        padding: 0.5em;
        border-top: solid 1px;
    }
    .popup-box > a : last-child{
        border-radius: 0 0 0.375rem 0.375rem;
    }
    .popup-box > a:hover {
        background-color: var(--contrast);
        color: rgb(255 255 255 / var(--tw-text-opacity));
    }
`

export default function Header() {
    const user = useContext(UserContext)
    const [showMenu, setShowMenu] = useState(false)

    const onClose = () => {
        setShowMenu(false)
    }

    return (
        <header className="header sticky top-0 z-50">
            <div className="header-top">
                <small>Contact-us +33.6.71.66.24.26</small>
            </div>
            <nav className="flex justify-between place-items-center max-w-6xl mx-5 xl:mx-auto h-24">
                {/* Left */}
                <div>
                    <div>English</div>
                    <div className="separator">
                    </div>
                    <ThemeSwitcher />
                </div>

                {/* Middle */}
                <div className='absolute left-0 right-0 m-auto text-center w-24 cursor-pointer top-12'>
                    <img src='/images/logo.png' layout='fill' />
                </div>

                {/* Right */}
                {user ? (
                    <MenuNav className='flex items-center justify-end space-x-4 relative'>
                        <Button onClick={() => setShowMenu(true)}>Menu</Button>
                        <Popup show={showMenu} onClose={onClose}>
                            <LoginButton />
                            {(hasRole('ROLE_TESTER')) && (
                                <Link href="/user/profile">My profile</Link>
                            )}
                            {(hasRole('ROLE_TESTER')) && (
                                <Link href="/user/campaigns">My campaigns</Link>
                            )}
                            {(hasRole('ROLE_ADMIN')) && (
                                <Link href="/admin">Admin</Link>
                            )}
                            {(hasRole('ROLE_CUSTOMER')) && (
                                <Link href="/customers/products">Products</Link>
                            )}
                            {(hasRole('ROLE_CUSTOMER')) && (
                                <Link href="/customers/campaigns">Campaigns</Link>
                            )}
                            <Link href="/">Home</Link>
                            <Link href="/whatwedo">What we do</Link>
                            <Link href="/whoweare">who we are</Link>
                            <Link href="/">New nomadic labgraded device & app</Link>
                            <Link href="/">Xcientific validation</Link>
                        </Popup>
                    </MenuNav>
                ) : (
                    <LoginButton />
                )}
            </nav>
        </header>
    )
}
