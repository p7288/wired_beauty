import NextLink from 'next/link'
import { Button } from './Button'

export default function Link ({ href, button = false, children, ...props }) {
    if (button) {
        return (
            <NextLink href={href}>
                <a {...props}>
                    <Button>
                        {children}
                    </Button>
                </a>
            </NextLink>
        )
    }
    return (
        <NextLink href={href}>
            <a {...props}>
                {children}
            </a>
        </NextLink>
    )
}