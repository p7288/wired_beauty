import { Spinner } from "./Animation/Spinner"

export function Button ({ children, loading = false, ...props }) {
    return (
        <button type="button" disabled={loading} className="group relative flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" { ...props}>
            {loading ?
                <Spinner />
            :
                children
            }
        </button>
    )
}

export function SubmitButton ({ children, loading = false, ...props }) {
    return (
        <button type="submit" disabled={loading} className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" { ...props}>
            {loading ?
                <Spinner />
            :
                children
            }
        </button>
    )
}