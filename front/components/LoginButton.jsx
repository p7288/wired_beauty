import { useContext } from "react"
import { userService } from "services/user.service"
import Link from "components/Link"
import { Button } from "components/Button"
import { UserContext } from "providers/UserContextProvider"

export default function LoginButton () {
    const user = useContext(UserContext)

    const logout = () => {
        userService.logout()
    }
    
    if (user) {
        return (
            <Button onClick={ logout }>Sign Out</Button>
        )
    }

    return (
        <Link href="/login" exact="true" button>Sign In</Link>
    )
}