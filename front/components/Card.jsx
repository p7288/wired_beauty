import { Icon } from "components/Icon";
import styled from 'styled-components';
import Link from './Link';

const CardHeader = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
`

export function Card({ children, ...props }) {
    const href = typeof(props.href)!=="undefined" ? props.href : "#";
    return (
        <Link href={href} className="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <CardHeader>
                {typeof(props.icon)!=="undefined"&& (<Icon name={props.icon} />)}
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{props.title}</h5>
            </CardHeader>
            <div className='card-body'>
                {children}
            </div>
        </Link>
    )
}