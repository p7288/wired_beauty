import React from 'react';
import { Button } from 'components/Button'

export const Pagination = ({ currentPage, itemsPerPage, length, onPageChanged }) => {

    const pagesCount = Math.ceil(length / itemsPerPage); 
    const pages = [];

    //pages counter
    for(let i = 1; i <= pagesCount; i++){
        pages.push(i);
    }
   
    return (
        <div className="flex justify-center">
                    <nav aria-label="Page navigation example">
                        <ul className="flex list-style-none">
                            <li className="page-item">
                                {
                                    currentPage === 1 
                                    ?
                                    <Button
                                        className="!page-link !relative !block !py-1.5 px-3 !rounded !border-0 !bg-transparent !outline-none !transition-all !duration-300 rounded !text-gray-800 !hover:text-gray-800 !hover:bg-gray-200 !focus:shadow-none disabled:opacity-25 " onClick={() => onPageChanged(currentPage - 1)}
                                        aria-label="Previous" disabled>
                                        <span aria-hidden="true">&laquo;</span>
                                    </Button>
                                    :
                                    <Button
                                        className="!page-link !relative !block !py-1.5 px-3 !rounded !border-0 !bg-transparent !outline-none !transition-all !duration-300 rounded !text-gray-800 !hover:text-gray-800 !hover:bg-gray-200 !focus:shadow-none " onClick={() => onPageChanged(currentPage - 1)}
                                        aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </Button>
                                }
                            </li>
                            {pages.map(page => (
                            <li className="page-item" key={page}>
                                <Button
                                    onClick={() => onPageChanged(page)} 
                                    className={"!page-link !relative !block !py-1.5 px-3 !rounded !border-0 !bg-transparent !outline-none !transition-all !duration-300 rounded !text-gray-800 !hover:text-gray-800 !hover:bg-gray-200 !focus:shadow-none "+ (currentPage === page && "!bg-secondary")}
                                >
                                        {page}
                                </Button>
                            </li>      
                            ))} 
                            <li className="page-item">
                                {
                                     currentPage === pagesCount 
                                     ?
                                     <Button
                                        className="!page-link !relative !block !py-1.5 px-3 !rounded !border-0 !bg-transparent !outline-none !transition-all !duration-300 rounded !text-gray-800 !hover:text-gray-800 !hover:bg-gray-200 !focus:shadow-none disabled:opacity-25" 
                                        onClick={() => onPageChanged(currentPage + 1)} 
                                        aria-label="Next"
                                        disabled
                                    >
                                        <span aria-hidden="true">&raquo;</span>
                                    </Button>
                                    :
                                    <Button
                                        className="!page-link !relative !block !py-1.5 px-3 !rounded !border-0 !bg-transparent !outline-none !transition-all !duration-300 rounded !text-gray-800 !hover:text-gray-800 !hover:bg-gray-200 !focus:shadow-none " 
                                        onClick={() => onPageChanged(currentPage + 1)} 
                                        aria-label="Next"
                                    >
                                        <span aria-hidden="true">&raquo;</span>
                                    </Button>
                                }
                            </li>
                        </ul>
                    </nav>
                </div>
    );
}

Pagination.getData = (items, currentPage, itemsPerPage) => {
    const start = currentPage * itemsPerPage - itemsPerPage;
    return items.slice(start, start + itemsPerPage); 
}