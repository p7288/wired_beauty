import { hasRole, isAuthenticated } from "functions/auth";
import { useEffect, useContext, useState } from "react"
import Router from 'next/router'
import { UserContext } from "providers/UserContextProvider"
import { AlertContext } from "providers/AlertContextProvider";

export default function AuthGuard ({ role, children }) {
    const user = useContext(UserContext)
    const alerts = useContext(AlertContext)
    const [accessDenied, setAccessDenied] = useState(false)
    
    useEffect(() => {
        if (!isAuthenticated()) {
            Router.push(Router.route)
            alerts.addAlert('warning', "You must be logged", 5, () => {})
            Router.push('/login')
        }
        if (role) {
            if (!hasRole(role)) {
                setAccessDenied(true)
            }
        }
    }, [user])

    if (accessDenied) {
        return (
            <h2>Access denied.</h2>
        )
    }

    return (<>{children}</>)
}
