import React, { createContext, useEffect, useLayoutEffect, useState, useContext, useMemo } from 'react'
import { ApiError, jsonFetch } from '/functions/api.js'
import { Button, SubmitButton } from '/components/Button.jsx'
import { classNames } from '/functions/dom.js'
import { AlertContext } from "providers/AlertContextProvider"
import styled from "styled-components"

/**
 * Représente un champs, dans le contexte du formulaire
 *
 * @param {string} type
 * @param {string} name
 * @param {function} onInput
 * @param {string} value
 * @param {string} error
 * @param {boolean} autofocus
 * @param {function} component
 * @param {React.Children} children
 * @param {string} className
 * @param {string} wrapperClass
 * @param props
 */
export function Field ({
  name,
  label,
  onInput,
  value,
  error,
  children,
  customLabelClass,
  type = 'text',
  className = '',
  wrapperClass = '',
  component = null,
  ...props
}) {
  // Hooks
  const [dirty, setDirty] = useState(false)
  const showError = error && !dirty

  function handleInput (e) {
    if (dirty === false) {
      setDirty(true)
    }
    if (onInput) {
      onInput(e)
    }
  }

  // Si le champs a une erreur et n'a pas été modifié
  if (showError) {
    className += ' is-invalid'
  }

  // Les attributs à passer aux champs
  const attr = {
    name,
    id: name,
    className,
    onInput: handleInput,
    type,
    ...(value === undefined ? {} : { value }),
    ...props
  }

  // On trouve le composant à utiliser
  const FieldComponent = useMemo(() => {
    if (component) {
      return component
    }
    switch (type) {
      case 'textarea':
        return FieldTextarea
      case 'checkbox':
        return FieldCheckbox
      default:
        return FieldInput
    }
  }, [component, type])

  // Si l'erreur change on considère le champs comme "clean"
  useLayoutEffect(() => {
    setDirty(false)
  }, [error])

  if (FieldComponent === FieldCheckbox) {
    return (
      <div className={`form-check ${wrapperClass}`}>
        <FieldComponent {...attr} />
        {children && (
          <label htmlFor={name} className='form-check-label'>
            {children}
          </label>
        )}
        {showError && <div className='invalid-feedback'>{error}</div>}
      </div>
    )
  }

  return (
    <FormGroup className={`form-group ${wrapperClass}`}>
      {children && <label htmlFor={name} className={`${customLabelClass ?? ''}`}>{children}</label>}
      <FieldComponent {...attr} />
      {showError && <div className='invalid-feedback'>{error}</div>}
    </FormGroup>
  )
}

/**
 * Bouton radio avec un label sur le côté
 */
export function Radio ({ children, ...props }) {
  return (
    <div className="flex justify-between place-items-center max-w-6xl mx-5 xl:mx-auto h-24">
      <span className={classNames('form-radio', props.checked && 'is-checked')}>
        <input type='radio' {...props} />
      </span>
      <label htmlFor={props.id} className='flex'>
        {children}
      </label>
    </div>
  )
}

/**
 * Bouton checkbox avec un label sur le côté
 */
export function Checkbox ({ children, ...props }) {
  return (
    <div className="flex justify-between place-items-center max-w-6xl mx-5 xl:mx-auto h-24">
      <span className={classNameNames('form-checkbox', props.checked && 'is-checked')}>
        <input type='checkbox' {...props} />
      </span>
      <label htmlFor={props.id} className='flex'>
        {children}
      </label>
    </div>
  )
}

function FieldTextarea (props) {
  return <textarea {...props} />
}

function FieldInput (props) {
    return <input {...props} />
}

function FieldCheckbox (props) {
  return <input {...props} />
}

/**
 * Version contextualisée des champs pour le formulaire
 */

export const FormContext = createContext({
  errors: {},
  loading: false,
  emptyError: () => {},
  addError: () => {},
})

/**
 * Formulaire Ajax
 *
 * @param {object} value Donnée à transmettre au serveur et aux champs
 * @param onChange
 * @param className
 * @param children
 * @param {string} action URL de l'action à appeler pour traiter le formulaire
 * @param {object} data Données à envoyer à l'API et à fusionner avec les données du formulaire
 * @param {string} method Méthode d'envoie des données
 * @param {bool} floatingAlert
 * @param onSuccess Fonction appelée en cas de retour valide de l'API (reçoit les données de l'API en paramètre)
 */
export function FetchForm ({
  data = {},
  children,
  action,
  className,
  contentType,
  method = 'POST',
  onChange = () => {},
  onSubmit = () => {},
  onSuccess = () => {},
}) {
    const alerts = useContext(AlertContext)
    const [{ loading, errors }, setState] = useState({
        loading: false,
        errors: []
    })

    useEffect(() => {
        errors.forEach(error => {
            alerts.addAlert('danger', error, 5, emptyErrors)
        })
    }, [errors])

    // Vide toutes les erreurs
    const emptyErrors = () => {
        setState(s => ({ ...s, errors: [] }))
    }

    // Vide l'erreur associée à un champs donnée
    const emptyError = name => {
        if (!errors[name]) return null
        const newErrors = { ...errors }
        delete newErrors[name]
        setState(s => ({ ...s, errors: newErrors }))
    }

    // Vide l'erreur associée à un champs donnée
    const addError = (name, error) => {
        const newErrors = { ...errors }
        newErrors[name] = error
        setState(s => ({ ...s, errors: newErrors }))
    }

    const handleChange = (e) => {
        onChange(e)
    }

    // On soumet le formulaire au travers d'une requête Ajax
    const handleSubmit = async e => {
        e.preventDefault()
        const form = e.target
        let formData = { ...data, ...Object.fromEntries(new FormData(form)) }
        for (var propertyName in formData) {
            if (parseFloat(formData[propertyName]) && !formData[propertyName].includes('-')) {
                formData[propertyName] = parseFloat(formData[propertyName])
            }

            if (formData[propertyName] === 'true' || formData[propertyName] === 'on'){
                formData[propertyName] = true;
            }

            if (formData[propertyName] === 'false'){
                formData[propertyName] = false;
            }
        }

        if (contentType === 'multipart/form-data') {
          const campaignId = formData.campaignId
          const file = formData.file

          formData = new FormData()
          formData.append('campaignId', campaignId)
          formData.append('file', file)
        }

        onSubmit(formData)
        setState({ loading: true, errors: [] })
        try {
            const response = await jsonFetch(action, { method, body: formData, contentType: contentType })
            // const isValid = await validationSchema.validate(formData, { strict: true, abortEarly: false })
            onSuccess(response)
            form.reset()
        } catch (e) {
            if (e instanceof ApiError) {
                setState(s => ({ ...s, errors: e.violations }))
            } else if (e.detail) {
                alerts.addAlert('danger', e.detail, 5, () => {})
            } else {
                alerts.addAlert('danger', e, 5, () => {})
                throw e
            }
        }
        setState(s => ({ ...s, loading: false }))
    }

    return (
        <FormContext.Provider value={{ loading, errors, emptyError, addError }}>
            <form onChange={ handleChange } onSubmit={handleSubmit} className={className}>
                {children}
            </form>
        </FormContext.Provider>
    )
}

/**
 * Représente un champs, dans le contexte du formulaire
 *
 * @param {string} type
 * @param {string} name
 * @param {React.Children} children
 * @param {object} props
 */
export function FormField ({ label, type = 'text', name, children, ...props }) {
    const { errors, emptyError, loading } = useContext(FormContext)
    const error = errors[name] || null

    return (
        <Field label={label} type={type} name={name} error={error} onInput={() => emptyError(name)} readOnly={loading} {...props}>
            {children}
        </Field>
    )
}

 export function FormSelect ({ name, value, label, onChange, children}) {

  return (
    <div>
      <label htmlFor={name}>{label}</label>
      <select onChange={onChange} name={name} id={name} value={value} className={`appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 rounded-t-md rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm`}>
        {children}
      </select>
    </div>
  )
}

/**
 * Représente un bouton, dans le contexte du formulaire
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
export function FormButton ({ children, ...props }) {
    const { loading, errors } = useContext(FormContext)
    const disabled = loading || Object.keys(errors).filter(k => k !== 'main').length > 0

    return (
        <Button loading={loading} disabled={disabled} {...props}>
            {children}
        </Button>
    )
}

/**
 * Représente un bouton, dans le contexte du formulaire
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
export function FormSubmitButton ({ children, ...props }) {
    const { loading, errors } = useContext(FormContext)
    const disabled = loading || Object.keys(errors).filter(k => k !== 'main').length > 0

    return (
        <SubmitButton loading={loading} disabled={disabled} {...props}>
            {children}
        </SubmitButton>
    )
}

const FormGroup = styled.div`
    margin: 1rem 0;

    input,
    textarea,
    select {
        border: 1px solid var(--border);
        background: var(--background);
        border-radius: 3px;
        color: var(--color);
        padding: space(1.5) space(1);
        display: block;
        width: 100%;
        outline: none;
        min-height: 48px;
    }
`