import Alert from "./Alert"

export default function AlertFloating ({ type = 'success', children, duration, onClose = () => {}, ...props }) {
    return (
        <Alert type={ type } duration={ duration } isFloating={ true } onClose={onClose} {...props}>{ children }</Alert>
    )
}