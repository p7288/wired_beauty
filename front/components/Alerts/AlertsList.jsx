import { useState, useEffect } from "react"
import Alert from "./Alert"
import AlertFloating from "./AlertFloating"

export default function AlertsList (props) {
    const [alertsList, setAlertsList] = useState([])

    const addAlertHandler = (type = 'success', content, duration = 5, handleClose = () => {}) => {
        const onClose = (id) => {
            setAlertsList(alertsList.filter(a => a.id !== id))
            handleClose()
        }
        const newAlert = {id: alertsList.length + 1, type, content, duration, onClose}
        setAlertsList([...alertsList, newAlert])
    }
    
    props.biRef.addAlertHandler = addAlertHandler

    return (
        <div className="alerts">
            { alertsList.map((alert, key) =>
                <AlertFloating key={key} type={ alert.type } duration={ alert.duration } onClose={() => alert.onClose(alert.id)}>{ alert.content }</AlertFloating>
            ) }
        </div>
    )
}