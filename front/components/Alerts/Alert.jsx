import { useEffect, useState } from "react"

export default function Alert ({ type = 'success', children, duration, isFloating = false, onClose = () => {} }) {
    const [isClosing, setClosing] = useState(false)
    const [isClosed, setClosed] = useState(false)

    useEffect(() => {
        if (duration) {
            setTimeout(handleClose, duration * 1000)
        }
    }, [])

    const handleClose = () => {
        if (isClosed) return
        setClosing(true)
        setTimeout(async () => {
            setClosed(true)
        }, 500)
        onClose()
    }
    
    const icon = () => {
        if (type === 'danger') {
            return 'alert-circle'
        } else if (type === 'warning') {
            return 'alert-triangle'
        } else if (type === 'success') {
            return 'check-square'
        } else if (type === 'info') {
            return 'info'
        }
        return type
    }

    if (isClosed) {
        return <></>
    }

    return (
        <div type={ type } className={ `full card alert alert-${type}${isClosing ? ' out' : ''}${isFloating ? ' is-floating' : ''}` } duration={ type === 'success' ? duration : undefined }>
            <svg className={ `icon icon-${icon()}` }>
                <use xlinkHref={ `/sprite.svg#${icon()}` }></use>
            </svg>
            <div>
                { children }
            </div>
            <button className="alert-close" onClick={ handleClose }>
                <svg className="icon">
                    <use xlinkHref="/sprite.svg#x"></use>
                </svg>
            </button>
            { duration && (
                <div className="alert__progress" style={{ animationDuration: duration + 's' }}></div>
            ) }
        </div>
    )
}  