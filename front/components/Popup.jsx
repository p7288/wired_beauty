import { useEffect, useRef, useState } from 'react'
import ReactDOM from 'react-dom'
import { Icon } from './Icon.jsx'
import { useClickOutside } from '../functions/hooks.js'

export function Popup ({ children, show = true, onClose, padding, style, className }) {
    const [shouldRender, setRender] = useState(show)
    const ref = useRef(null);
    useClickOutside(ref, () => {
        if (shouldRender) {
            if (typeof onClose === 'function') {
                onClose()
            } else {
                ref.current.classList.add('out')
            }
        }
    })

    useEffect(() => {
        if (show) setRender(true)
    }, [show])

    useEffect(() => {
        document.body.style.overflow = "auto"
        if (shouldRender) {
            document.body.style.overflow = "hidden"
        }
    }, [shouldRender])
    
    const onAnimationEnd = e => {
        if (typeof onClose === 'function') {
            if (!show && e.animationName === 'popupOut') setRender(false)
        } else {
            if (e.animationName === 'popupOut') setRender(false)
        }
    }

    useEffect(() => {      
        const handler = e => {
            if (e.key === 'Escape' && shouldRender) {
                if (typeof onClose === 'function') {
                    onClose()
                } else {
                    ref.current.classList.add('out')
                }
            }
        }
      
        window.addEventListener("keyup", handler);
        return () => window.removeEventListener("keyup", handler)
    }, [shouldRender])

    if (!shouldRender) {
        return null
    }

    return (
        <div
            className='popup-dialog'
            style={{ animation: `${show ? 'popupIn' : 'popupOut'} .3s both` }}
            onAnimationEnd={onAnimationEnd}
            ref={ref}
        >
            <div className="popup-box">
                {children}
            </div>
        </div>
    )
}