import styled from "styled-components"

export function Spinner ({ margin = 10 }) {
    const style = {
        margin: `${margin}px auto`
    }
    
    return <LoadingSpinner className="loading-spinner" style={ style }>
        {[...Array(3)].map((item, key) => 
            <span key={key} />
        )}
    </LoadingSpinner>
}

const LoadingSpinner = styled.div`
    --size: 18px;

    display: flex;
    width: fit-content;
    gap: 3px;

    > span {
        background: var(--color);
        border-radius: 2px;
        width: calc(var(--size) / 3);
        height: calc(var(--size) / 1);
        animation: stretch 1s ease-in-out infinite;
    }
    > span:nth-of-type(2) {
        animation-delay: .15s;
    }
    > span:nth-of-type(3) {
        animation-delay: .3s;
    }

    @keyframes stretch {
        60% {
            transform: scaleY(1);
        }
        30% {
            transform: scaleY(1.6);
        }
    }
`