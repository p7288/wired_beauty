import { useState, useEffect, useContext } from "react"
import { Button } from "components/Button"
import { AlertContext } from "providers/AlertContextProvider"

export default function ThemeSwitcher () {
    const alerts = useContext(AlertContext)
    const [theme, setTheme] = useState('light')

    useEffect(() => {
        const localTheme = window.localStorage.getItem('theme')
        if (localTheme) {
            var newTheme = localTheme
        } else {
            const prefersDark = window.matchMedia("(prefers-color-scheme: dark)").matches
            var newTheme = prefersDark ? 'dark' : 'light'
        }
        document.querySelector('body').classList.add(newTheme)
        setTheme(newTheme)
    }, [])

    const toggleTheme = () => {
        document.querySelector('body').classList.remove(theme)
        const newTheme = theme === 'light' ? 'dark' : 'light'
        document.querySelector('body').classList.add(newTheme)
        setTheme(newTheme)
        localStorage.setItem('theme', newTheme)
    }

    return (
        <div>
            <Button onClick={ toggleTheme }>Change Theme</Button>
        </div>
    )
}