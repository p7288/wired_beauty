import React, { createContext, useEffect, useLayoutEffect, useState, useContext, useMemo } from 'react'
import { ApiError, jsonFetch } from '/functions/api.js'
import { AppContext } from 'providers/AppContextProvider'
import styled from "styled-components"
import { Spinner } from './Animation/Spinner'
import { AlertContext } from 'providers/AlertContextProvider'

/**
 * Version contextualisée pour la table
 */
export const TableContext = createContext({
    errors: {},
    loading: false,
    emptyError: () => {},
    addError: () => {},
})
  
/**
 * Table Ajax
 *
 * @param {object} value Donnée à transmettre au serveur et aux champs
 * @param onChange
 * @param className
 * @param children
 * @param {string} action URL de l'action à appeler
 * @param {bool} floatingAlert
 * @param onSuccess Fonction appelée en cas de retour valide de l'API (reçoit les données de l'API en paramètre)
 */
export function FetchTable ({
    children,
    action,
    className,
    onSuccess = () => {}
}) {
    const alerts = useContext(AlertContext)
    const [{ loading, errors }, setState] = useState({
        loading: false,
        errors: []
    })

    useEffect(() => {
        if(errors.length > 0){
            errors.forEach(error => {
                alerts.addAlert('danger', error, 5, emptyErrors)
            })
        }
    }, [errors])

    // Vide toutes les erreurs
    const emptyErrors = () => {
        setState(s => ({ ...s, errors: [] }))
    }

    // Vide l'erreur associée à un champs donnée
    const emptyError = name => {
        if (!errors[name]) return null
        const newErrors = { ...errors }
        delete newErrors[name]
        setState(s => ({ ...s, errors: newErrors }))
    }

    // Vide l'erreur associée à un champs donnée
    const addError = (name, error) => {
        const newErrors = { ...errors }
        newErrors[name] = error
        setState(s => ({ ...s, errors: newErrors }))
    }

    // On charge les éléments dès l'affichage du composant
    useEffect(async () => {
        setState({ loading: true, errors: [] })
        try {
            const response = await jsonFetch(action, { method: 'GET' })
            onSuccess(response)
        } catch (e) {
            if (e instanceof ApiError) {
                setState(s => ({ ...s, errors: e.violations }))
            } else if (e.detail) {
                alerts.addAlert('danger', e.detail, 5, () => {})
            } else {
                alerts.addAlert('danger', e, 5, () => {})
                throw e
            }
        }
        setState(s => ({ ...s, loading: false }))
    }, [])

    return (
        <TableContext.Provider value={{ loading, errors, emptyError, addError }}>
            <Table className={`${className} w-full text-sm text-left bg-primary shadow-lg rounded-md`}>
                {children}
            </Table>
        </TableContext.Provider>
    )
}

/**
 * Représente un head, dans le contexte de la table
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
 export function TableHead ({ children, ...props }) {
    return (
        <thead {...props} className="text-xs uppercase border-b-2 border-secondary">
            {children}
        </thead>
    )
}

/**
 * Représente un body, dans le contexte de la table
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
 export function TableBody ({ children, size = 3, ...props }) {
    const { loading } = useContext(TableContext)

    if (loading) {
        return (
            <tbody>
                <tr>
                    <td colSpan={size} style={{ paddingTop: 10 }}>
                        <Spinner />
                    </td>
                </tr>
            </tbody>
        )
    }

    return (
        <tbody {...props}>
            {children}
        </tbody>
    )
}

/**
 * Représente une ligne, dans le contexte de la table
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
 export function TableRow ({ children, ...props }) {
    return (
        <tr {...props}>
            {children}
        </tr>
    )
}

/**
 * Représente une colonne, dans le contexte de la table côté body
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
 export function TableCol ({ children, ...props }) {
    return (
        <td {...props} className={`${props.className} px-3 py-4 font-medium whitespace-nowrap`}>
            {children}
        </td>
    )
}

/**
 * Représente une colonne, dans le contexte de la table côté head
 *
 * @param children
 * @param props
 * @return {*}
 * @constructor
 */
 export function TableColHead ({ children, ...props }) {
    return (
        <th {...props} className={`${props.className} px-6 py-3`}>
            {children}
        </th>
    )
}

const Table = styled.table`
    width: 100%;

    tr {
        text-align: center;
    }

    td.flex{
        display: flex;
        flex-direction: row;
        justify-content: space-around;
    }
`
