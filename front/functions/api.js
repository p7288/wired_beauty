// import { flash } from '/elements/Alert.js'

import { userService } from "services/user.service"

export const HTTP_UNPROCESSABLE_ENTITY = 422
export const HTTP_NOT_FOUND = 404
export const HTTP_FORBIDDEN = 403
export const HTTP_OK = 200
export const HTTP_NO_CONTENT = 204

/**
 * @param {RequestInfo} url
 * @param params
 * @return {Promise<Object>}
 */
export async function jsonFetch(url, params = {}, checkExpiredJWT = true) {
    let headers = {}

    if (params.contentType == undefined) delete params.contentType

    if (checkExpiredJWT && userService.isExpiredJWT()) {
        await userService.refreshJWT()
    }

    // Si on reçoit un objet on le convertit en chaine JSON et différent d'un formData
    if (params.body && typeof params.body === 'object' && params.contentType !== 'multipart/form-data') {
        params.body = JSON.stringify(params.body)
    }

    if (params.contentType !== 'multipart/form-data') {
        headers = {
            'Content-Type': contentType(params),
            Accept: 'application/json'
        }
    }

    url = `${process.env.API_URI}${url}`
    params = {
        headers: {
            ...headers,
            ... await authHeader(url)
        },
        ...params
    }

    const response = await fetch(url, params)
    if (response.status === 204) {
        return response
    }
    const data = await response.json()
    if (response.ok) {
        return data
    }
    throw new ApiError(data, response.status)
}

/**
 * @param {RequestInfo} url
 * @param params
 * @return {Promise<Object>}
 */
export async function jsonFetchOrFlash (url, params = {}) {
    try {
        return await jsonFetch(url, params)
    } catch (e) {
        if (e instanceof ApiError) {
        flash(e.name, 'danger', 4)
        } else {
        flash(e, 'danger', 4)
        }
        return null
  }
}

/**
 * Capture un retour d'API
 *
 * @param {function} fn
 */
export async function catchViolations (p) {
    try {
        return [await p, null]
    } catch (e) {
        if (e instanceof ApiError) {
            return [null, e.violations]
        }
        throw e
    }
}

/**
 * Représente une erreur d'API
 * @property {{
 *  violations: {propertyPath: string, message: string}
 * }} data
 */
export class ApiError {
    constructor (data, status) {
        this.data = data
        this.status = status
    }

    // Récupère la liste de violation pour un champs donnée
    violationsFor (field) {
        return this.data.violations.filter(v => v.propertyPath === field).map(v => v.message)
    }

    get name () {
        return `${this.data.detail || this.data.message}`
    }

    // Renvoie les violations indexé par propertyPath
    get violations () {
        if (!this.data.violations) {
            return [
                `${this.data.detail || this.data.message}`
            ]
        }
        return this.data.violations.reduce((acc, violation) => {
            if (acc[violation.propertyPath]) {
                acc[violation.propertyPath].push(violation.message)
            } else {
                acc[violation.propertyPath] = [violation.message]
            }
            return acc
        }, {})
    }
}

async function authHeader(url) {
    // return auth header with jwt if user is logged in and request is to the api url
    const user = userService.userValue
    const isLoggedIn = user && user.token
    const isApiUrl = url.startsWith(process.env.API_URI)
    if (isLoggedIn && isApiUrl) {
        return { Authorization: `Bearer ${user.token}` }
    } else {
        return {}
    }
}

function contentType (params) {
    if (params.contentType) return params.contentType

    if (
        typeof(params.headers) !== "undefined" &&
        typeof(params.headers.method) !== "undefined" &&
        params.headers.method === "PATCH"
    ) {
        return 'application/merge-patch+json'
    }

    return 'application/json'
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text)

        if (!response.ok) {
            if ([401, 403].includes(response.status) && userService.userValue) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                userService.logout()
            }

            const error = (data && data.message) || response.statusText
            return Promise.reject(error)
        }

        return data
    })
}
