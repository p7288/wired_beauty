/**
 * Génère une classe à partir de différentes variables
 *
 * @param  {...string|null} classnames
 */
export function classNames(...classnames) {
  return classnames.filter((classname) => classname !== null && classname !== false).join(" ")
}

/**
 * Convertit les données d'un formulaire en objet JavaScript
 *
 * @param {HTMLFormElement} form
 * @return {{[p: string]: string}}
 */
export function formDataToObj(form) {
  return Object.fromEntries(new FormData(form))
}


export function toFormatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}