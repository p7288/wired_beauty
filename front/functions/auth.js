import { userService } from "services/user.service"

/**
 * Vérifie si l'utilisateur est connecté
 *
 * @return {boolean}
 */
 export function isAuthenticated () {
    const user = userService.userValue
    if (user && user.token) {
        return true
    }
    return false
}

/**
 * Vérifie si l'utilisateur est connecté
 *
 * @return {boolean}
 */
 export function hasRole (role) {
    if (!isAuthenticated()) {
        return false
    }
    const user = userService.userData
    return user.roles.includes(role)
}