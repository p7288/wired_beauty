echo "Install starting ------------------------------------------------------------------------------------------------------------"
echo "FRONTEND BUILD start ------------------------------------------------------------------------------------------------------------"

cd ./front
docker-compose build
docker-compose up -d

echo "FRONTEND BUILD end ------------------------------------------------------------------------------------------------------------"
echo "BACKEND BUILD start ------------------------------------------------------------------------------------------------------------"

cd ../back
docker-compose build

echo "BACKEND BUILD end ------------------------------------------------------------------------------------------------------------"

echo "BACKEND composer bundles install start ------------------------------------------------------------------------------------------------------------"

docker-compose run php composer install

echo "BACKEND composer bundles install end ------------------------------------------------------------------------------------------------------------"

docker-compose up -d
cd ..

echo "Install end ------------------------------------------------------------------------------------------------------------"

docker ps

echo "Frontend : localhost:3000 \nBackend localhost:8080 \nPhpmyadmin localhost:8081 \n\n Read README.md for front and back to have all informations of application"