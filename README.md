# wired_beauty

Projet annuel m2 ESGI

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/p7288/wired_beauty.git
git branch -M main
git push -uf origin main
```

## Install

* Run `sh install.sh` (you need docker)
* Manual installation => read README.md file on back and front folder

## Code analyses

* https://github.com/squizlabs/PHP_CodeSniffer