<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetMyCampaigns extends AbstractController
{
    public function __construct(private Security $security)
    {
        $this->security = $security;
    }

    public function __invoke($data,CampaignRepository $campaignRepository){
        $user = $this->security->getUser();
        if (in_array("ROLE_TESTER", $user->getRoles())) {
            $campaigns = $campaignRepository->findByTesterRegistered($user);
            if (sizeof($campaigns) > 0) {
                return $campaigns;
            }
        }
        return false;
    }
}