<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\SessionRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Message;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('/api', name: 'app_api:')]
class ApiController extends AbstractController
{
    public function __construct(private Security $security, private ManagerRegistry $doctrine)
    {
        $this->security = $security;
        $this->em = $doctrine->getManager();
    }

    #[Route('/register/{session_id}', name: 'registerSession')]
    public function registerSession(int $session_id, SessionRepository $sessionRepository): Response
    {
        $user = $this->security->getUser();
        $session = $sessionRepository->find($session_id);
        if (sizeof($session->getTesters()) < $session->getQuantity()) {
            $session->addTester($user);
            $this->em->persist($session);
            $this->em->flush();
            return new JsonResponse(['message' => 'Successfully registered', 'code' => '200'], 200);
        }
        // return new JsonResponse(['message' => $message, 'code' => '404'], 404);
        return new JsonResponse(['message' => 'No Session found maybe there are no more place try again', 'code' => '404'], 404);
    }
}
