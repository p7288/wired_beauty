<?php

namespace App\Controller;
use Doctrine\Common\Collections\ArrayCollection;


class GetAvailableController
{

    public function __invoke($data){
        $filtered = [];
        foreach( $data as $campaign){
            $sessions = $campaign->getSessions();
            foreach( $sessions as $session){
                if( $session->getQuantity() > sizeof($session->getTesters()) ){
                    $filtered[]=$campaign;
                }
            }
        }
        return new ArrayCollection($filtered);
    }
}