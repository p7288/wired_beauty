<?php

namespace App\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{
    private $encoder;
    private $userRepository;
    private $jwtManager;

    /**
     * AuthController constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param JWTTokenManagerInterface $jwtManager
     * @param ValidatorInterface $validator
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $jwtManager)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
        $this->jwtManager = $jwtManager;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $data = $request->toArray();

        try {
            $response = $this->userRepository->createNewUser($data);

            if ($response['error']) {
                return new JsonResponse($response);
            } else {
                $user = $response['user'];

                return new JsonResponse(
                    [
                        'code' => $response['code'],
                        'message' => $response['message'],
                        'token' => $this->jwtManager->create($user)
                    ]
                );
            }
        } catch (UniqueConstraintViolationException $uniqueConstraintViolationException) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'An error occurred. Email has already used');
        }
    }

    /**
    * api route redirects
    * @return Response
    */
    public function api(): Response
    {
        return new Response(sprintf("Logged in as %s", $this->getUser()->getUsername()));
    }
}
