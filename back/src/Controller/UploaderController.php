<?php

namespace App\Controller;

use App\Service\ImportQuestionsConfigFileService;
use App\Service\UploaderService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UploaderController extends AbstractController
{
    #[Route('/api/uploader/import-questions-config-file', name: 'uploader_import_questions_config_file')]
    /**
     * @param Request $request
     * @param UploaderService $uploaderService
     * @param ImportQuestionsConfigFileService $importQuestionsConfigFileService
     * @throws Exception
     * @return JsonResponse
     */
    public function importQuestionsConfigFile(
        Request $request,
        UploaderService $uploaderService,
        ImportQuestionsConfigFileService $importQuestionsConfigFileService
    ): JsonResponse {
        $campaignId = $request->request->get('campaignId');
        if (!$campaignId) {
            throw new NotFoundHttpException('Campaign ID is required');
        }

        $uploaderResponse = $uploaderService->uploadFile($request->files->get('file'), $this->getParameter('kernel.project_dir') . '/public/tmp');
        if ($uploaderResponse['error']) {
            throw new NotFoundHttpException($uploaderResponse['message']);
        }

        return new JsonResponse($importQuestionsConfigFileService->execute($uploaderResponse['filePath'], $campaignId));
    }
}
