<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Campaign;
use App\Entity\Customer;
use App\Entity\Feedback;
use App\Entity\Page;
use App\Entity\Product;
use App\Entity\Question;
use App\Entity\Session;
use App\Entity\Survey;
use App\Entity\Tester;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        /**
         * Admin
         */
        $admin = new Admin();
        $hash = $this->encoder->hashPassword($admin, 'admin1234');

        $admin->setEmail("admin@email.com")
            ->setPassword($hash)
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        /**
         * Page
         */
        for ($p = 0; $p < 3; $p++) {
            $page = new Page();
            $page->setContent($faker->realText())
                ->setTitle($faker->word)
                ->setAdmin($admin);

            $manager->persist($page);
        }

        /**
         * Surveys
         */
        for ($s = 0; $s < 10; $s++) {
            $survey = new Survey();
            $survey->setName($faker->name);

            $manager->persist($survey);

            /**
             * Questions
             */
            for ($q = 0; $q < 15; $q++) {
                $question = new Question();
                $question->setSurvey($survey)
                    ->setName($faker->name)
                    ->setPossibleResponses($faker->randomElements([$faker->words, $faker->words, $faker->words]));

                $manager->persist($question);
            }
        }

        /**
         * Testers
         */
        for ($t = 0; $t < 5; $t++) {
            $tester = new Tester();
            $hash = $this->encoder->hashPassword($tester, 'tester1234');
            $tester->setEmail("test$t@email.com")
                ->setPassword($hash)
                ->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
                ->setHeight($faker->randomFloat(2,150,210))
                ->setBirthDate($faker->dateTimeBetween('-5 years', 'now'))
                ->setLocation($faker->longitude)
                ->setRoles(['ROLE_TESTER'])
                ->setWeight($faker->randomFloat(2,50,150))
                ->setPosition($faker->address);

            $manager->persist($tester);
        }

        /**
         * Customers
         */
        for ($c = 0; $c < 5; $c++) {
            $customer = new Customer();
            $hash = $this->encoder->hashPassword($customer, 'customer1234');
            $customer->setBusinessName($faker->company)
                ->setEmail("customer$c@email.com")
                ->setPassword($hash)
                ->setRoles(['ROLE_CUSTOMER']);

            $manager->persist($customer);

            /**
             * Products
             */
            for ($p = 0; $p < 5; $p++) {
                $product = new Product();
                $product->setName($faker->name)
                    ->setDescription('test')
                    ->setCustomer($customer);

                $manager->persist($product);

                /**
                 * Campaigns
                 */
                for ($cp = 0; $cp < 5; $cp++) {
                    $campaign = new Campaign();
                    $campaign->setLabel($faker->word)
                        ->setProductQuantity(mt_rand(0, 15))
                        ->setTesterDesc('test')
                        ->setProduct($product)
                        ->setClientDesc('test')
                        ->setSurvey($survey)
                        ->setCustomer($customer)
                        ->setStatus($faker->randomElement([
                            Campaign::STATUS_VALIDATE,
                            Campaign::STATUS_IN_PROGRESS,
                            Campaign::STATUS_DONE
                        ]));

                    $manager->persist($campaign);

                    /**
                     * Sessions
                     */
                    for ($s = 0; $s < 5; $s++) {
                        $session = new Session();
                        $session->setDateBegin($faker->dateTimeBetween('-30 years', 'now'))
                            ->setDateEnd($faker->dateTimeBetween('-30 years', 'now'))
                            ->setInstructions('Instructions')
                            ->setIsPlacebo($faker->boolean())
                            ->setCampaign($campaign)
                            ->setQuantity(mt_rand(0, 50));

                        $manager->persist($session);

                        /**
                         * Feedbacks
                         */
                        for ($f = 0; $f < 10; $f++) {
                            $feedback = new Feedback();
                            $feedback->setTester($tester)
                                ->setSession($session)
                                ->setFeedback($faker->word);

                            $manager->persist($feedback);
                        }
                    }
                }
            }
        }
        $manager->flush();
    }
}
