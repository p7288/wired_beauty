<?php 

namespace App\Serializer;
use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
final class AdminGroupsContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationChecker;
    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }
    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        // $resourceClass = $context['resource_class'] ?? null;
        // if ( isset($context['groups']) && $this->authorizationChecker->isGranted('ROLE_ADMIN') && $request->query->get("groups")!==null) {
        if ( isset($context['groups']) && $request->query->get("groups")!==null) {
            $context['groups'] = $request->query->get("groups");
        }
        return $context;
    }
}