<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['questions:collection:read']],
    ],
    "post" => [
        "security" => "is_granted('ROLE_ADMIN')",
    ],
],
    itemOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['question:item:read:']],
    ],
    "put" => [
        "security" => "is_granted('ROLE_ADMIN')",
    ],
    "delete" => [
        "security" => "is_granted('ROLE_ADMIN')"
    ],
],
)]
#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['questions:collection:read','question:item:read:'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Survey::class, inversedBy: 'questions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['questions:collection:read','question:item:read:'])]
    private Survey $survey;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['questions:collection:read','question:item:read:'])]
    private string $name;

    #[ORM\Column(type: 'json')]
    #[Groups(['question:item:read:'])]
    private array $possibleResponses = [];

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: Response::class)]
    #[Groups(['question:item:read:'])]
    private Collection $responses;

    public function __construct()
    {
        $this->responses = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPossibleResponses(): ?array
    {
        return $this->possibleResponses;
    }

    public function setPossibleResponses(array $possibleResponses): self
    {
        $this->possibleResponses = $possibleResponses;

        return $this;
    }

    /**
     * @return Collection<int, Response>
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setQuestion($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->removeElement($response)) {
            // set the owning side to null (unless already changed)
            if ($response->getQuestion() === $this) {
                $response->setQuestion(null);
            }
        }

        return $this;
    }
}
