<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SurveyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        "get" => [
            "normalization_context" => ['groups' => ['surveys:collection:read']],
        ],
        "post" => [
            "security" => "is_granted('ROLE_CUSTOMER')",
        ],
    ],
    itemOperations: [
        "get" => [
            "normalization_context" => ['groups' => ['survey:item:read:']],
        ],
        "put" => [
            "security" => "is_granted('ROLE_CUSTOMER')",
        ],
        "delete" => [
            "security" => "is_granted('ROLE_CUSTOMER')"
        ],
    ],
)]
#[ORM\Entity(repositoryClass: SurveyRepository::class)]
class Survey
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['surveys:collection:read', 'survey:item:read:'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['campaign:list', 'surveys:collection:read', 'survey:item:read:'])]
    private string $name;

    #[ORM\OneToMany(mappedBy: 'survey', targetEntity: Campaign::class)]
    #[Groups(['survey:item:read:'])]
    private Collection $campaigns;

    #[ORM\OneToMany(mappedBy: 'survey', targetEntity: Question::class)]
    #[Groups(['survey:item:read:'])]
    private Collection $questions;

    public function __construct()
    {
        $this->campaigns = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Campaign>
     */
    public function getCampaigns(): Collection
    {
        return $this->campaigns;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns[] = $campaign;
            $campaign->setSurvey($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaigns->removeElement($campaign)) {
            // set the owning side to null (unless already changed)
            if ($campaign->getSurvey() === $this) {
                $campaign->setSurvey(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setSurvey($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getSurvey() === $this) {
                $question->setSurvey(null);
            }
        }

        return $this;
    }
}
