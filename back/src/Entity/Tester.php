<?php

namespace App\Entity;

use App\Repository\TesterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['testers:collection:read']],
    ],
    "post" => [
        "security" => "is_granted('ROLE_ADMIN')",
    ],
],
    itemOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['tester:item:read:']],
    ],
    "put" => [
        "security" => "is_granted('ROLE_ADMIN') or is_granted('ROLE_TESTER')",
    ],
    "delete" => [
        "security" => "is_granted('ROLE_ADMIN')"
    ],
],
)]
#[ORM\Entity(repositoryClass: TesterRepository::class)]
class Tester extends User
{
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['testers:collection:read','tester:item:read:','user:item'])]
    private string $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['testers:collection:read','tester:item:read:','user:item'])]
    private string $lastname;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['testers:collection:read','tester:item:read:','user:item'])]
    private ?\DateTimeInterface $birthDate;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(['testers:collection:read','tester:item:read','user:item'])]
    private ?float $weight;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(['testers:collection:read','tester:item:read','user:item'])]
    private ?float $height;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['testers:collection:read','tester:item:read','user:item'])]
    private ?string $position;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['testers:collection:read','tester:item:read','user:item'])]
    private ?string $location;

    #[ORM\OneToMany(mappedBy: 'tester', targetEntity: Feedback::class)]
    #[Groups(['tester:item:read:','user:item'])]
    private Collection $feedbacks;

    #[ORM\ManyToMany(targetEntity: Session::class, mappedBy: 'testers')]
    private $sessions;

    public function __construct()
    {
        $this->feedbacks = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection<int, Feedback>
     */
    public function getFeedbacks(): Collection
    {
        return $this->feedbacks;
    }

    public function addFeedback(Feedback $feedback): self
    {
        if (!$this->feedbacks->contains($feedback)) {
            $this->feedbacks[] = $feedback;
            $feedback->setTester($this);
        }

        return $this;
    }

    public function removeFeedback(Feedback $feedback): self
    {
        if ($this->feedbacks->removeElement($feedback)) {
            // set the owning side to null (unless already changed)
            if ($feedback->getTester() === $this) {
                $feedback->setTester(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Session>
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->addTester($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            $session->removeTester($this);
        }

        return $this;
    }

        /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_TESTER';

        return array_unique($roles);
    }
}
