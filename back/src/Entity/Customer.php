<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['customers:collection:read']],
    ],
    "post" => [
        "security" => "is_granted('ROLE_ADMIN')",
    ],
],
    itemOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['customer:item:read:']],
    ],
    "put" => [
        "security" => "is_granted('ROLE_ADMIN')",
    ],
    "delete" => [
        "security" => "is_granted('ROLE_ADMIN')"
    ],
],
)]
#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer extends User
{
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['campaign:list', 'campaign:item', "campaign:admin-view",'user:item'])]
    private string $business_name;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Product::class)]
    #[Groups(['campaign:item'])]
    private Collection $products;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Campaign::class)]
    private $campaigns;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
    }

    public function getBusinessName(): string
    {
        return $this->business_name;
    }

    public function setBusinessName(string $business_name): self
    {
        $this->business_name = $business_name;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setCustomer($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCustomer() === $this) {
                $product->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Campaign>
     */
    public function getCampaigns(): Collection
    {
        return $this->campaigns;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns[] = $campaign;
            $campaign->setCustomer($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaigns->removeElement($campaign)) {
            // set the owning side to null (unless already changed)
            if ($campaign->getCustomer() === $this) {
                $campaign->setCustomer(null);
            }
        }

        return $this;
    }
    
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_CUSTOMER';

        return array_unique($roles);
    }
}
