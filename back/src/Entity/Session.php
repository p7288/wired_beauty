<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        "get" => [
            "normalization_context" => ['groups' => ['sessions:collection:read']],
        ],
        "post" => [
            "security" => "is_granted('ROLE_CUSTOMER')",
        ],
    ],
    itemOperations: [
        "get" => [
            "normalization_context" => ['groups' => ['session:item:read']],
        ],
        "put" => [
            "security" => "is_granted('ROLE_CUSTOMER')",
        ],
        "delete" => [
            "security" => "is_granted('ROLE_CUSTOMER')"
        ],
    ],
)]
#[ORM\Entity(repositoryClass: SessionRepository::class)]
class Session
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['campaign:item','campaign:list', 'sessions:collection:read', 'session:item:read'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Campaign::class, inversedBy: 'sessions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['session:item:read', 'sessions:collection:read'])]
    private Campaign $campaign;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['campaign:item', 'sessions:collection:read', 'session:item:read'])]
    private \DateTimeInterface $dateBegin;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['campaign:item', 'sessions:collection:read', 'session:item:read'])]
    private \DateTimeInterface $dateEnd;

    #[ORM\Column(type: 'integer')]
    #[Groups(['campaign:item', 'sessions:collection:read', 'session:item:read'])]
    private int $quantity = 1;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['campaign:item', 'sessions:collection:read', 'session:item:read'])]
    private ?string $instructions;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['campaign:item', 'sessions:collection:read', 'session:item:read'])]
    private bool $isPlacebo = false;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: Feedback::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['session:item:read'])]
    private Collection $feedbacks;

    #[ORM\ManyToMany(targetEntity: Tester::class, inversedBy: 'sessions')]
    private $testers;

    public function __construct()
    {
        $this->feedbacks = new ArrayCollection();
        $this->testers = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCampaign(): Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getDateBegin(): \DateTimeInterface
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\DateTimeInterface $dateBegin): self
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    public function getDateEnd(): \DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(?string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getIsPlacebo(): ?bool
    {
        return $this->isPlacebo;
    }

    public function setIsPlacebo(bool $isPlacebo): self
    {
        $this->isPlacebo = $isPlacebo;

        return $this;
    }

    /**
     * @return Collection<int, Feedback>
     */
    public function getFeedbacks(): Collection
    {
        return $this->feedbacks;
    }

    public function addFeedback(Feedback $feedback): self
    {
        if (!$this->feedbacks->contains($feedback)) {
            $this->feedbacks[] = $feedback;
            $feedback->setSession($this);
        }

        return $this;
    }

    public function removeFeedback(Feedback $feedback): self
    {
        if ($this->feedbacks->removeElement($feedback)) {
            // set the owning side to null (unless already changed)
            if ($feedback->getSession() === $this) {
                $feedback->setSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tester>
     */
    public function getTesters(): Collection
    {
        return $this->testers;
    }

    public function addTester(Tester $tester): self
    {
        if (!$this->testers->contains($tester)) {
            $this->testers[] = $tester;
        }

        return $this;
    }

    public function removeTester(Tester $tester): self
    {
        $this->testers->removeElement($tester);

        return $this;
    }
}
