<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\GetAvailableController;
use App\Controller\GetMyCampaigns;
use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['campaign:list']],
        ],
        "post" => ["security" => "is_granted('ROLE_CUSTOMER')"],
        "availableCampaign" => [
            'method' => 'GET',
            'path' => '/campaigns/getavailable',
            'controller' => GetAvailableController::class,
            'normalization_context' => ['groups' => ['campaign:list']],
        ],
        "myCampaigns" => [
            'method' => 'GET',
            'path' => '/campaigns/mycampaigns',
            'controller' => GetMyCampaigns::class,
            'normalization_context' => ['groups' => ['campaign:admin-view']],
        ]
    ],
    itemOperations: [
        "get" => [
            'normalization_context' => ['groups' => ['campaign:item']],
        ],
        "put" => ["security" => "is_granted('ROLE_CUSTOMER')"],
        "delete" => ["security" => "is_granted('ROLE_CUSTOMER')"]
    ],
)]
#[ApiFilter(SearchFilter::class, properties: ['customer' => 'exact'])]
#[ORM\Entity(repositoryClass: CampaignRepository::class)]
class Campaign
{
    private const STATUS = ['to_validate', 'done', 'in_progress'];
    public const STATUS_VALIDATE = self::STATUS[0];
    public const STATUS_DONE = self::STATUS[1];
    public const STATUS_IN_PROGRESS = self::STATUS[2];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view", 'session:item:read', 'sessions:collection:read'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'campaigns')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    private Product $product;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Choice([self::STATUS_DONE, self::STATUS_IN_PROGRESS, self::STATUS_VALIDATE])]
    #[Assert\NotBlank]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view", 'session:item:read'])]
    private string $status = self::STATUS[0];

    #[ORM\Column(type: 'integer')]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    private int $productQuantity = 1;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 0, max: 255)]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    private string $label;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    private string $clientDesc;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    private string $testerDesc;

    #[ORM\OneToMany(mappedBy: 'campaign', targetEntity: Session::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(["campaign:list", "campaign:item", "campaign:admin-view"])]
    #[ORM\JoinColumn(referencedColumnName: "id")]
    private Collection $sessions;

    #[ORM\ManyToOne(targetEntity: Survey::class, inversedBy: 'campaigns')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(["campaign:admin-view"])]
    private ?Survey $survey;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'campaigns')]
    private $customer;

    #[ORM\ManyToMany(targetEntity: Tester::class, mappedBy: 'campaigns')]
    private $testers;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->testers = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->productQuantity;
    }

    public function setProductQuantity(int $productQuantity): self
    {
        $this->productQuantity = $productQuantity;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getClientDesc(): ?string
    {
        return $this->clientDesc;
    }

    public function setClientDesc(string $clientDesc): self
    {
        $this->clientDesc = $clientDesc;

        return $this;
    }

    public function getTesterDesc(): ?string
    {
        return $this->testerDesc;
    }

    public function setTesterDesc(string $testerDesc): self
    {
        $this->testerDesc = $testerDesc;

        return $this;
    }

    /**
     * @return Collection<int, Session>
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setCampaign($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getCampaign() === $this) {
                $session->setCampaign(null);
            }
        }

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, Tester>
     */
    public function getTesters(): Collection
    {
        return $this->testers;
    }

    public function addTester(Tester $tester): self
    {
        if (!$this->testers->contains($tester)) {
            $this->testers[] = $tester;
            $tester->addCampaign($this);
        }

        return $this;
    }

    public function removeTester(Tester $tester): self
    {
        if ($this->testers->removeElement($tester)) {
            $tester->removeCampaign($this);
        }

        return $this;
    }
}
