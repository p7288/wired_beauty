<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FeedbackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['feedbacks:collection:read']],
    ],
    "post" => [
        "security" => "is_granted('ROLE_TESTER')",
    ],
],
    itemOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['feedback:item:read:']],
    ],
    "put" => [
        "security" => "is_granted('ROLE_TESTER')",
    ],
    "delete" => [
        "security" => "is_granted('ROLE_ADMIN')"
    ],
],
)]
#[ORM\Entity(repositoryClass: FeedbackRepository::class)]
class Feedback
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['tester:item:read'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Tester::class, inversedBy: 'feedbacks')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['tester:item:read'])]
    private Tester $tester;

    #[ORM\ManyToOne(targetEntity: Session::class, inversedBy: 'feedbacks')]
    #[ORM\JoinColumn(nullable: false)]
    private Session $session;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255)]
    #[Groups(['feedbacks:collection:read','feedback:item:read'])]
    private ?string $feedback;

    #[ORM\OneToMany(mappedBy: 'feedback', targetEntity: Response::class)]
    #[Groups(['tester:item:read'])]
    private Collection $responses;

    public function __construct()
    {
        $this->responses = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTester(): Tester
    {
        return $this->tester;
    }

    public function setTester(Tester $tester): self
    {
        $this->tester = $tester;

        return $this;
    }

    public function getSession(): Session
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return Collection<int, Response>
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(Response $response): self
    {
        if (!$this->responses->contains($response)) {
            $this->responses[] = $response;
            $response->setFeedback($this);
        }

        return $this;
    }

    public function removeResponse(Response $response): self
    {
        if ($this->responses->removeElement($response)) {
            // set the owning side to null (unless already changed)
            if ($response->getFeedback() === $this) {
                $response->setFeedback(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFeedback(): ?string
    {
        return $this->feedback;
    }

    /**
     * @param string $feedback
     */
    public function setFeedback(?string $feedback): self
    {
        $this->feedback = $feedback;
        return $this;
    }
}
