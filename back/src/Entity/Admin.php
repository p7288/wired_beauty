<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AdminRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['admin:list']],
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "post" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
    ],
    itemOperations: [
        "get" => [
            'normalization_context' => ['groups' => ['admin:item']],
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "put" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "delete" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
)]
#[ORM\Entity(repositoryClass: AdminRepository::class)]
class Admin extends User
{
    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: Page::class)]
    private Collection $pages;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    /**
     * @return Collection<int, Page>
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setAdmin($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->removeElement($page)) {
            // set the owning side to null (unless already changed)
            if ($page->getAdmin() === $this) {
                $page->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_ADMIN';

        return array_unique($roles);
    }
}
