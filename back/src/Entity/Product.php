<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['product:list']],
        ],
        "post" => ["security" => "is_granted('ROLE_CUSTOMER')"],
    ],
    itemOperations: [
        "get" => [
            'normalization_context' => ['groups' => ['product:item']],
        ],
        "put" => ["security" => "is_granted('ROLE_CUSTOMER')"],
        "delete" => ["security" => "is_granted('ROLE_CUSTOMER')"]
    ],
)]
#[ApiFilter(SearchFilter::class, properties: ['customer' => 'exact'])]
#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["campaign:item", "product:list", "product:item"])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(["campaign:list", "campaign:item", "product:list", "product:item","campaign:admin-view"])]
    private string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Groups(["campaign:item", "product:list", "product:item"])]
    private ?string $description;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Campaign::class)]
    private Collection $campaigns;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["product:list", "product:item","campaign:admin-view"])]
    private Customer $customer;

    #[ORM\ManyToMany(targetEntity: Tag::class, mappedBy: 'products')]
    #[Groups(["campaign:list"])]
    private $tags;

    public function __construct()
    {
        $this->campaigns = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Campaign>
     */
    public function getCampaigns(): Collection
    {
        return $this->campaigns;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns[] = $campaign;
            $campaign->setProduct($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaigns->removeElement($campaign)) {
            // set the owning side to null (unless already changed)
            if ($campaign->getProduct() === $this) {
                $campaign->setProduct(null);
            }
        }

        return $this;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addProduct($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeProduct($this);
        }

        return $this;
    }
}
