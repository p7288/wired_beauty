<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ResponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['responses:collection:read']],
    ],
    "post" => [
        "security" => "is_granted('ROLE_TESTER')",
    ],
],
    itemOperations: [
    "get" => [
        "normalization_context" => ['groups' => ['response:item:read:']],
    ],
    "put" => [
        "security" => "is_granted('ROLE_TESTER')",
    ],
    "delete" => [
        "security" => "is_granted('ROLE_TESTER')"
    ],
],
)]
#[ORM\Entity(repositoryClass: ResponseRepository::class)]
class Response
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['responses:collection:read','response:item:read:'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Feedback::class, inversedBy: 'responses')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['reponses:collection:read','reponse:item:read:'])]
    private Feedback $feedback;

    #[ORM\ManyToOne(targetEntity: Question::class, inversedBy: 'responses')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['reponse:item:read:'])]
    private Question $question;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255)]
    #[Groups(['reponses:collection:read','reponse:item:read:'])]
    private string $value;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFeedback(): Feedback
    {
        return $this->feedback;
    }

    public function setFeedback(Feedback $feedback): self
    {
        $this->feedback = $feedback;

        return $this;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function setQuestion(Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
