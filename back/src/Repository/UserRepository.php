<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Admin;
use App\Entity\Customer;
use App\Entity\Tester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    private $manager;
    private $encoder;
    private $security;
    private $validator;

    public function __construct(ManagerRegistry $registry, UserPasswordEncoderInterface $encoder, Security $security, ValidatorInterface $validator)
    {
        parent::__construct($registry, User::class);
        $this->manager = $this->getEntityManager();
        $this->encoder = $encoder;
        $this->security = $security;
        $this->validator = $validator;
    }

    /**
     * Create a new user
     * @param $data
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
    */
    public function createNewUser($data): array
    {
        switch (strtolower($data['type'])) {
            case User::TYPE_ADMIN:
                if (!is_null($this->security->getUser()) && $this->security->getUser()->isAdmin()) {
                    $user = new Admin();
                } else {
                    return ['error' => true, 'code' => 401, 'message' => 'You are not authorized to create an admin account.'];
                }
                break;
            case User::TYPE_CUSTOMER:
                $user = new Customer();
                if (!isset($data['business_name'])) {
                    return ['error' => true, 'code' => 400, 'message' => 'Bad request : missing business name !'];
                }
                $user->setBusinessName($data['business_name']);
                break;
            case User::TYPE_TESTER:
                $user = new Tester();
                if (!isset($data['firstname']) || !isset($data['lastname'])) {
                    return ['error' => true, 'code' => 400, 'message' => 'Bad request : missing first name or last name !'];
                }
                $user->setFirstname($data['firstname']);
                $user->setLastname($data['lastname']);
                break;
            default:
                return ['error' => true, 'code' => 400, 'message' => 'Bad request : bad account type value !'];
        }
        if (!isset($data['email']) || !isset($data['password']) || !$user) {
            return ['error' => true, 'code' => 400, 'message' => 'Bad request : missing email or password !'];
        }

        $user->setEmail($data['email'])->setPassword($this->encoder->encodePassword($user, $data['password']));

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, "Please check your information. {$errors[0]->getMessage()}");
        }

        $this->manager->persist($user);
        $this->manager->flush();

        return [
            'error' => false,
            'code' => 200,
            'message' => sprintf('User %s successfully created', $user->getUsername()),
            'user' => $user
        ];
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf("Instances of '%s' are not supported.", \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }


    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(User $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(User $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
