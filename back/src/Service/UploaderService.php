<?php

namespace App\Service;

use DateTime;
use Exception;

/**
 * Class UploaderService
 * @package App\Service
 */
class UploaderService
{
    /**
     * @param $file
     * @return array
     * @throws Exception
     */
    public function uploadFile($file, $path): array
    {
        if (empty($file)) {
            return ['error' => true, 'message' => 'No file specified'];
        }

        $dateTime = new DateTime();
        $filename = $file->getClientOriginalName();
        $file->move($path, $filename);

        return ['error' => false, 'message' => 'Upload success', 'filePath' => "$path/$filename"];
    }
}
