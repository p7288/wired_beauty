<?php

namespace App\Service;

use App\Entity\Campaign;
use App\Entity\Question;
use App\Entity\Survey;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ImportQuestionsConfigFileService
 * @package App\Service
 */
class ImportQuestionsConfigFileService
{
    private $entityManager;

    /**
     * ImportQuestionsConfigFileService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $filePath
     * @param int $campaignId
     * @return array
     */
    public function execute(string $filePath, int $campaignId): array
    {
        try {
            $campaign = $this->entityManager->getRepository(Campaign::class)->find((int) $campaignId);

            if (!$campaign) {
                throw new NotFoundHttpException('Campaign not found');
            }

            $spreadsheet = IOFactory::load($filePath);
            $sheet = $spreadsheet->getActiveSheet();

            $validationResponse = $this->validateSheet($sheet);
            if ($validationResponse['error']) {
                return $validationResponse;
            }

            $survey = $this->createSurvey($sheet->getTitle());
            $survey->addCampaign($campaign);

            foreach ($sheet->toArray() as $dataQuestions) {
                $dataQuestions = array_filter($dataQuestions);
                if (empty($dataQuestions)) {
                    continue;
                }

                $question = $this->createQuestion($dataQuestions);
                $survey->addQuestion($question);
                $this->entityManager->persist($survey);
            }

            $this->entityManager->flush();

            unlink($filePath);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            return ['error' => true, 'message' => 'Error loading file, please retry.'];
        }

        return ['error' => false, 'message' => 'Import success'];
    }

    /**
     * @param Worksheet $sheet
     * @return array
     */
    public function validateSheet(Worksheet $sheet): array
    {
        $error = null;

        if (!$sheet->getTitle()) {
            $error = 'Sheet has not title, please add title (for survey name)';
        }

        return ['error' => !empty($error), 'message' => $error];
    }

    /**
     * @param string $surveyName
     * @return Survey $survey
     */
    public function createSurvey(string $surveyName): Survey
    {
        $survey = new Survey();
        $survey->setName($surveyName);

        return $survey;
    }

    /**
     * @param array $dataQuestions
     * @return Question
     */
    public function createQuestion(array $dataQuestions): Question
    {
        $question = new Question();
        $questionName = array_shift($dataQuestions);

        $question->setName($questionName);
        $question->setPossibleResponses($dataQuestions);

        $this->entityManager->persist($question);

        return $question;
    }
}
