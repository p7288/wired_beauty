# Installation

* `docker-compose build`
* `docker-compose run php composer install`
* `docker-compose up -d`
* `docker-compose exec php php bin/console d:d:c (General error: 1007 Can't create database 'db'; database exists)`
* Check Jwt-authentication part after

# Command line

* `docker ps` (you must have 3 containers)
* `docker-compose exec php` before all command (install bundles with composer for example)

# LOGS web server

* In ./logs folder

# Phpmyadmin

* `http://localhost:8081/`
* Host `mysql`
* User `user`
* Password `password`

# Jwt-authentication

* First be sure that openssl is installed then run 
* `php bin/console lexik:jwt:generate-keypair` (or with docker-compose exec php)

# Create admin user command
* Run this command and replace "email" and "password" by valid input
* `php bin/console app:create-admin "email" "password"`

# Install dev en (clear:cache + update shemas + run fixtures)
* `composer install-dev`

# Users 
-   admin@email.com -> admin1234
-   tester{i<5}@email.com -> tester1234
-   customer{i<5]@email.com -> customer1234

