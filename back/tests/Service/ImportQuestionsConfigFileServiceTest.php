<?php

namespace App\Service;

use App\Entity\Question;
use App\Entity\Survey;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\TestCase;

class ImportQuestionsConfigFileServiceTest extends TestCase
{
    public function testConstruct()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $subject = new ImportQuestionsConfigFileService($entityManager);

        $this->assertInstanceOf(ImportQuestionsConfigFileService::class, $subject);
    }

    public function testExecute()
    {
        $this->markTestIncomplete();
    }

    public function testValidateSheet()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $subject = new ImportQuestionsConfigFileService($entityManager);

        $worksheet = $this->createMock(Worksheet::class);
        $this->assertSame(['error' => true, 'message' => 'Sheet has not title, please add title (for survey name)'], $subject->validateSheet($worksheet));

        $worksheet->method('getTitle')->willReturn('toto');
        $this->assertSame(['error' => false, 'message' => null], $subject->validateSheet($worksheet));
    }

    public function testCreateSurvey()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $subject = new ImportQuestionsConfigFileService($entityManager);

        $survey = $subject->createSurvey('toto');

        $this->assertInstanceOf(Survey::class, $survey);
        $this->assertSame('toto', $survey->getName());
    }

    public function testCreateQuestion()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $subject = new ImportQuestionsConfigFileService($entityManager);

        $question = $subject->createQuestion(['question1', 'reponse1', 'reponse2']);
        $this->assertInstanceOf(Question::class, $question);
        $this->assertSame('question1', $question->getName());
        $this->assertSame(['reponse1', 'reponse2'], $question->getPossibleResponses());
    }
}
