<?php

namespace App\Service;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File;

class UploaderServiceTest extends TestCase
{
    public function testUploadFile()
    {
        $subject = new UploaderService();
        $this->assertSame(['error' => true, 'message' => 'No file specified'], $subject->uploadFile(null));

        # ToDo fix me => need to mock DateTime
        #$file = $this->createMock(File\UploadedFile::class);
        #$file->method('getClientOriginalName')->willReturn('toto.xlsx');
        #$file->method('move')->willReturn(true);

        #$this->assertSame(['error' => false, 'message' => 'Upload success', 'filePath' => "./tmp/toto.xlsx"], $subject->uploadFile($file));
    }
}
